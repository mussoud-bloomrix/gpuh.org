<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AdminAuth\LoginController;
use App\Http\Controllers\AdminAuth\ForgotPasswordController;
use App\Http\Controllers\AdminAuth\ResetPasswordController;
use App\Http\Controllers\AdminAuth\ChangePasswordController;
use App\Http\Controllers\Admin\HomeController;
use App\Http\Controllers\Frontend\PageController;
use App\Http\Controllers\Admin\CMS\InitiativeController;
use App\Http\Controllers\Admin\CMS\CampaignController;
use App\Http\Controllers\Admin\CMS\NewsController;
use App\Http\Controllers\Admin\CMS\PressReleaseController;
use App\Http\Controllers\Admin\CMS\SiteSettingController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('frontend.index');
// });



Route::get('/',  [PageController::class, 'index'])->name('home');
Route::get('/what-we-do', [PageController::class, 'whatWeDo'])->name('pages.what-we-do');
Route::get('/who-we-are', [PageController::class, 'whoWeAre'])->name('pages.who-we-are');
Route::get('policy', [PageController::class, 'policy'])->name('pages.policy');
Route::get('/initiatives', [PageController::class, 'initiatives'])->name('pages.initiatives');
Route::get('/campaigns', [PageController::class, 'campaigns'])->name('pages.campaigns');
// Route::get('/press-releases', [PageController::class, 'pressReleases'])->name('pages.press-releases');
// Route::get('/in-the-news', [PageController::class, 'inTheNewsListing'])->name('pages.in-the-news');
Route::get('/latest-news', [PageController::class, 'latestNewsListing'])->name('pages.latest-news');


Route::get('/who-we-are', [PageController::class, 'whoWeAre'])->name('pages.who-we-are');
Route::get('privacy-policy', [PageController::class, 'privacyPolicy'])->name('pages.privacy.policy');
Route::get('cookie-policy', [PageController::class, 'cookiePolicy'])->name('pages.cookie.policy');
Route::get('terms-of-use', [PageController::class, 'termsOfUse'])->name('pages.terms.of.use');
Route::get('/press-releases/{media:slug}', [PageController::class, 'pressRelease'])->name('pages.press-release');
Route::get('/in-the-news/{media:slug}', [PageController::class, 'inTheNews'])->name('pages.in-the-news-detail');
Route::get('/latest-news/{media:slug}', [PageController::class, 'latestNews'])->name('pages.latest-news-detail');

Route::get('initiatives/{initiative:slug}', [PageController::class, 'initiative'])->name('pages.initiative');
Route::get('initiatives/{initiative:slug}/{campaign:slug}', [PageController::class, 'campaign'])->name('pages.campaign');
Route::get('initiatives/{initiative:slug}/{campaign:slug}/{media:slug}', [PageController::class, 'mediaCenterDetail'])->name('pages.media-center-detail');

Route::get('/contact', [PageController::class, 'contact'])->name('pages.contact');
Route::post('/contact', [PageController::class, 'contactMail'])->name('pages.contact-mail');


// Auth::routes();
Route::group(['prefix' => 'laravel-filemanager', 'middleware' => ['auth:admin']], function () {
    \UniSharp\LaravelFilemanager\Lfm::routes();
});

Route::prefix('/admin')->name('admin.')->group(function () {
    Route::get('login', [LoginController::class, 'showLoginForm'])->name('login');
    Route::post('login', [LoginController::class, 'login'])->name('login');
    // password reset routes
    Route::post('/password/email', [ForgotPasswordController::class, 'sendResetLinkEmail'])->name('password.email');
    Route::get('/password/reset', [ForgotPasswordController::class, 'showLinkRequestForm'])->name('password.request');
    Route::post('/password/reset', [ResetPasswordController::class, 'reset'])->name('password.update');
    Route::get('password/reset/{token}', [ResetPasswordController::class, 'showResetForm'])->name('password.reset');
});

Route::prefix('/admin')->middleware('auth:admin')->name('admin.')->group(function () {
    Route::get('/', [HomeController::class, 'index'])->name('home');
    Route::post('logout', [LoginController::class, 'logout'])->name('logout');
    Route::get('/change-password', [ChangePasswordController::class, 'changePassword'])->name('change-password.edit');
    Route::put('/change-password', [ChangePasswordController::class, 'updatePassword'])->name('change-password.update');
    Route::resource('initiatives', InitiativeController::class)->except(['show']);
    Route::resource('initiatives.campaigns', CampaignController::class)->except(['show']);
    Route::resource('press-releases', PressReleaseController::class);
    Route::resource('news', NewsController::class);
});

Route::prefix('/admin')->middleware('auth:admin')->name('admin.')->group(function () {
    Route::get('site-settings/edit', [SiteSettingController::class, 'edit'])->name('site-settings.edit');
    Route::post('site-settings/store', [SiteSettingController::class, 'store'])->name('site-settings.store');
    Route::put('site-settings/{generalSettings}/update', [SiteSettingController::class, 'update'])->name('site-settings.update');
});
