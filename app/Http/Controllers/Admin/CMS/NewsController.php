<?php

namespace App\Http\Controllers\Admin\CMS;

use App\Http\Controllers\Controller;
use App\Models\Campaign;
use App\Models\CampaignPost;
use App\Models\News;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $news = News::all();
        return view('admin.cms.news.index', compact('news'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $campaigns = Campaign::all();
        return view('admin.cms.news.create', compact('campaigns'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'title' => 'required|string|max:255|unique:press_releases,title',
            'excerpt' => 'nullable|string',
            'image' => 'required_without:video_url|image',
            'video_url' => 'required_without:image',
            'introduction' => 'nullable|string',
            'slider_images' => 'nullable|array',
            'slider_images.*' => 'image',
            'type' => 'required|string',
            'campaigns' => 'nullable|array|min:1',
            'campaigns.*' => 'nullable|numeric|exists:campaigns,id',
            'date' => 'nullable|date',
            'featured_image' => 'required|image',
            'header_image' => 'required|image',
            'body' => 'nullable|string',
            'meta_title' => 'required|string',
            'meta_keywords' => 'nullable',
            'meta_description' => 'nullable',
        ]);
        if ($request->slider_images) {
            $images = [];
            $sliderImages = $request->file('slider_images');
            foreach ($sliderImages as $sliderImage) {
                $images[] = uploadFile($sliderImage);
            }
            $data['slider_images'] = json_encode($images);
        }

        $data['header_image'] = uploadFile($request->file('header_image'));
        if ($request->image) {
            $data['image'] = uploadFile($request->file('image'));
        }
        $data['featured_image'] = uploadFile($request->file('featured_image'));
        $data['slug'] = Str::slug($data['title']);
        $news = News::create(collect($data)->except('campaigns')->toArray());
        if ($request->campaigns) {
            foreach ($data['campaigns'] as $campaignId) {
                CampaignPost::create([
                    'campaign_id' => $campaignId,
                    'postable_type' => News::class, 'postable_id' => $news->id
                ]);
            }
        }
        $request->session()->flash('message', 'News created successfully');
        $request->session()->flash('alert-class', 'alert alert-success');
        return redirect(route('admin.news.index'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\News $news
     * @return \Illuminate\Http\Response
     */
    public function edit(News $news)
    {
        $campaigns = Campaign::all();
        return view('admin.cms.news.edit', compact('campaigns', 'news'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\News $news
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, News $news)
    {
        $data = $request->validate([
            'title' => 'required|string|max:255|unique:press_releases,title,' . $news->id,
            'excerpt' => 'nullable|string',
            'video_url' => 'nullable|url',
            'image' => 'nullable|image',
            'introduction' => 'nullable|string',
            'slider_images' => 'nullable|array',
            'slider_images.*' => 'image',
            'campaigns' => 'nullable|array|min:1',
            'type' => 'nullable|string',
            'campaigns.*' => 'required|numeric|exists:campaigns,id',
            'date' => 'nullable|date',
            'featured_image' => 'nullable|image',
            'header_image' => 'nullable|image',
            'body' => 'nullable|string',
            'meta_title' => 'required|string',
            'meta_keywords' => 'nullable',
            'meta_description' => 'nullable',
        ]);

        $data['slider_images'] = $news->slider_images;

        if ($request->slider_images) {
            $images = [];
            $sliderImages = $request->file('slider_images');
            foreach ($sliderImages as $sliderImage) {
                $images[] = uploadFile($sliderImage);
            }
            $data['slider_images'] = json_encode($images);
        }

        $data['header_image'] = $news->header_image;

        if ($request->header_image) {
            $data['header_image'] = uploadFile($request->file('header_image'));
        }

        $data['image'] = $news->image;

        if ($request->image) {
            $data['image'] = uploadFile($request->file('image'));
        }

        $data['featured_image'] = $news->featured_image;
        if ($request->featured_image) {
            $data['featured_image'] = uploadFile($request->file('featured_image'));
        }

        $data['slug'] = Str::slug($data['title']);

        $news->update(collect($data)->except('campaigns')->toArray());
        if ($request->campaigns) {
            $news->campaigns()->detach();
            foreach ($data['campaigns'] as $campaignId) {
                CampaignPost::create([
                    'campaign_id' => $campaignId,
                    'postable_type' => News::class, 'postable_id' => $news->id
                ]);
            }
        }

        $request->session()->flash('message', 'News updated successfully');
        $request->session()->flash('alert-class', 'alert alert-success');
        return redirect(route('admin.news.edit', $news));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\News $news
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, News $news)
    {
        Storage::delete('public/' . $news->featured_image);
        Storage::delete('public/' . $news->header_image);
        $news->campaigns()->detach();
        $news->delete();
        $request->session()->flash('message', 'News deleted successfully');
        $request->session()->flash('alert-class', 'alert alert-danger');
        return redirect(route('admin.news.index'));
    }
}
