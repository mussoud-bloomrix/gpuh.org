<?php

namespace App\Http\Controllers\Admin\CMS;

use App\Http\Controllers\Controller;
use App\Models\Campaign;
use App\Models\CampaignPost;
use App\Models\PressRelease;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class PressReleaseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pressReleases = PressRelease::all();
        return view('admin.cms.press-releases.index', compact('pressReleases'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $campaigns = Campaign::all();
        return view('admin.cms.press-releases.create', compact('campaigns'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'title' => 'required|string|max:255|unique:press_releases,title',
            'excerpt' => 'required|string',
            'image' => 'required_without:video_url|image',
            'video_url' => 'required_without:image',
            'introduction' => 'required|string',
            'slider_images' => 'required|array',
            'slider_images.*' => 'image',
            'campaigns' => 'required|array|min:1',
            'campaigns.*' => 'required|numeric|exists:campaigns,id',
            'date' => 'required|date',
            'featured_image' => 'required|image',
            'header_image' => 'required|image',
            'body' => 'required|string',
            'meta_title' => 'required|string',
            'meta_keywords' => 'nullable',
            'meta_description' => 'nullable',
        ]);

        $images = [];
        $sliderImages = $request->file('slider_images');
        foreach ($sliderImages as $sliderImage) {
            $images[] = uploadFile($sliderImage);
        }
        $data['slider_images'] = json_encode($images);

        if ($request->image) {
            $data['image'] = uploadFile($request->file('image'));
        }

        $data['header_image'] = uploadFile($request->file('header_image'));
        $data['featured_image'] = uploadFile($request->file('featured_image'));
        $data['slug'] = Str::slug($data['title']);

        $pressRelease = PressRelease::create(collect($data)->except('campaigns')->toArray());

        foreach ($data['campaigns'] as $campaignId) {
            CampaignPost::create([
                'campaign_id' => $campaignId,
                'postable_type' => PressRelease::class, 'postable_id' => $pressRelease->id
            ]);
        }

        $request->session()->flash('message', 'Press releases created successfully');
        $request->session()->flash('alert-class', 'alert alert-success');
        return redirect(route('admin.press-releases.index'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\PressRelease  $pressRelease
     * @return \Illuminate\Http\Response
     */
    public function edit(PressRelease $pressRelease)
    {
        $campaigns = Campaign::all();
        return view('admin.cms.press-releases.edit', compact('campaigns', 'pressRelease'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\PressRelease  $pressRelease
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PressRelease $pressRelease)
    {
        $data = $request->validate([
            'title' => 'required|string|max:255|unique:press_releases,title,' . $pressRelease->id,
            'excerpt' => 'required|string',
            'video_url' => 'nullable|url',
            'image' => 'nullable|image',
            'introduction' => 'required|string',
            'slider_images' => 'nullable|array',
            'slider_images.*' => 'image',
            'campaigns' => 'required|array|min:1',
            'campaigns.*' => 'required|numeric|exists:campaigns,id',
            'date' => 'required|date',
            'featured_image' => 'nullable|image',
            'header_image' => 'nullable|image',
            'body' => 'required|string',
            'meta_title' => 'required|string',
            'meta_keywords' => 'nullable',
            'meta_description' => 'nullable',
        ]);

        $data['slider_images'] = $pressRelease->slider_images;

        if ($request->slider_images) {
            $images = [];
            $sliderImages = $request->file('slider_images');
            foreach ($sliderImages as $sliderImage) {
                $images[] = uploadFile($sliderImage);
            }
            $data['slider_images'] = json_encode($images);
        }

        $data['image'] = $pressRelease->image;

        if ($request->image) {
            $data['image'] = uploadFile($request->file('image'));
        }

        $data['header_image'] = $pressRelease->header_image;

        if ($request->header_image) {
            $data['header_image'] = uploadFile($request->file('header_image'));
        }

        $data['featured_image'] = $pressRelease->featured_image;
        if ($request->featured_image) {
            $data['featured_image'] = uploadFile($request->file('featured_image'));
        }

        $data['slug'] = Str::slug($data['title']);

        $pressRelease->update(collect($data)->except('campaigns')->toArray());
        $pressRelease->campaigns()->detach();

        foreach ($data['campaigns'] as $campaignId) {
            CampaignPost::create([
                'campaign_id' => $campaignId,
                'postable_type' => PressRelease::class, 'postable_id' => $pressRelease->id
            ]);
        }

        $request->session()->flash('message', 'Press release updated successfully');
        $request->session()->flash('alert-class', 'alert alert-success');
        return redirect(route('admin.press-releases.edit', $pressRelease));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\PressRelease  $pressRelease
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, PressRelease $pressRelease)
    {
        Storage::delete('public/' . $pressRelease->featured_image);
        Storage::delete('public/' . $pressRelease->header_image);
        $pressRelease->campaigns()->detach();
        $pressRelease->delete();
        $request->session()->flash('message', 'Press release deleted successfully');
        $request->session()->flash('alert-class', 'alert alert-danger');
        return redirect(route('admin.press-releases.index'));
    }
}
