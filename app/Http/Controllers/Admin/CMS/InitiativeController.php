<?php

namespace App\Http\Controllers\Admin\CMS;

use App\Http\Controllers\Controller;
use App\Models\Initiative;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class InitiativeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $initiatives = Initiative::all();
        return view('admin.cms.initiatives.index', compact('initiatives'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.cms.initiatives.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'name' => 'required|string|max:255',
            'header_image' => 'required|image',
            'feature_image' => 'required|image',
            'excerpt' => 'required|string',
            'body' => 'required',
            'meta_title' => 'required',
            'meta_keywords' => 'nullable',
            'meta_description' => 'nullable',
        ]);

        $data['header_image'] = uploadFile($request->file('header_image'));
        $data['feature_image'] = uploadFile($request->file('feature_image'));

        $data['slug'] = \Str::slug($data['name']);
        Initiative::create($data);
        $request->session()->flash('message', 'Initiative created successfully');
        $request->session()->flash('alert-class', 'alert alert-success');
        return redirect(route('admin.initiatives.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Initiative $initiative)
    {
        return view('admin.cms.initiatives.edit', compact('initiative'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Initiative $initiative)
    {
        $data = $request->validate([
            'name' => 'required|string|max:255',
            'header_image' => 'nullable|image',
            'feature_image' => 'nullable|image',
            'excerpt' => 'required|string',
            'body' => 'required',
            'meta_title' => 'required',
            'meta_keywords' => 'nullable',
            'meta_description' => 'nullable',
        ]);

        if ($request->header_image) {
            $data['header_image'] = uploadFile($request->file('header_image'));
        } else {
            $data['header_image'] = $initiative->header_image;
        }

        if ($request->feature_image) {
            $data['feature_image'] = uploadFile($request->file('feature_image'));
        } else {
            $data['feature_image'] = $initiative->feature_image;
        }

        $data['slug'] = \Str::slug($data['name']);

        $initiative->update($data);
        $request->session()->flash('message', 'Initiative updated successfully');
        $request->session()->flash('alert-class', 'alert alert-success');
        return redirect(route('admin.initiatives.index'));
    }

    public function destroy(Initiative $initiative)
    {
        Storage::delete('public/' . $initiative->header_image);
        $initiative->delete();
        $request->session()->flash('message', 'Initiative deleted successfully');
        $request->session()->flash('alert-class', 'alert alert-danger');
        return redirect(route('admin.initiatives.index'));
    }
}
