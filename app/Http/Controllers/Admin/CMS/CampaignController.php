<?php

namespace App\Http\Controllers\Admin\CMS;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Campaign;
use App\Models\Initiative;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;

class CampaignController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Initiative $initiative)
    {
        $campaigns = $initiative->campaigns;
        return view('admin.cms.campaigns.index', compact('campaigns', 'initiative'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Initiative $initiative)
    {
        return view('admin.cms.campaigns.create', compact('initiative'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Initiative $initiative)
    {
        $data = $request->validate([
            'name' => ['required', 'string', 'max:255', Rule::unique('campaigns')->where(function ($query) use ($initiative) {
                return $query->where('initiative_id', $initiative->id);
            })],
            'header_image' => 'required|image',
            'feature_image' => 'required|image',
            'excerpt' => 'required|string',
            'body' => 'required',
            'meta_title' => 'required',
            'meta_keywords' => 'nullable',
            'meta_description' => 'nullable',
        ]);

        $data['header_image'] = uploadFile($request->file('header_image'));
        $data['feature_image'] = uploadFile($request->file('feature_image'));

        $data['slug'] = \Str::slug($data['name']);
        $initiative->campaigns()->create($data);
        $request->session()->flash('message', 'Campaign created successfully');
        $request->session()->flash('alert-class', 'alert alert-success');
        return redirect()->route('admin.initiatives.campaigns.index', $initiative->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Initiative $initiative, Campaign $campaign)
    {
        return view('admin.cms.campaigns.edit', compact('campaign', 'initiative'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Initiative $initiative, Campaign $campaign)
    {
        $data = $request->validate([
            'name' => ['required', 'string', 'max:255', Rule::unique('campaigns')->where(function ($query) use ($initiative) {
                return $query->where('initiative_id', $initiative->id);
            })->ignore($campaign->id, 'id')],
            'header_image' => 'nullable|image',
            'feature_image' => 'nullable|image',
            'excerpt' => 'required|string',
            'body' => 'required',
            'meta_title' => 'required',
            'meta_keywords' => 'nullable',
            'meta_description' => 'nullable',
        ]);

        if ($request->header_image) {
            $data['header_image'] = uploadFile($request->file('header_image'));
        } else {
            $data['header_image'] = $campaign->header_image;
        }

        if ($request->feature_image) {
            $data['feature_image'] = uploadFile($request->file('feature_image'));
        } else {
            $data['feature_image'] = $campaign->feature_image;
        }

        $data['slug'] = \Str::slug($data['name']);

        $campaign->update($data);
        $request->session()->flash('message', 'Campaign updated successfully');
        $request->session()->flash('alert-class', 'alert alert-success');
        return redirect()->back();
    }

    public function destroy(Request $request, Initiative $initiative, Campaign $campaign)
    {
        Storage::delete('public/' . $campaign->header_image);
        $campaign->pressReleases()->detach();
        $campaign->delete();
        $request->session()->flash('message', 'Campaign deleted successfully');
        $request->session()->flash('alert-class', 'alert alert-danger');
        return redirect(route('admin.initiatives.campaigns.index', [$initiative]));
    }
}
