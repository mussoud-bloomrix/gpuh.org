<?php

namespace App\Http\Controllers\Admin\CMS;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\SiteSetting;

class SiteSettingController extends Controller
{

    /**
     * Edit the form for editing the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        $siteSettings = SiteSetting::first();
        if ($siteSettings) {
            return view('admin.cms.site-settings.edit', compact('siteSettings'));
        } else {
            return view('admin.cms.site-settings.create');
        }
    }

    /**
     * Store the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $settings = SiteSetting::first();
        if ($settings) {
            update($request, $settings);
        }

        $data = $request->validate([
            'logo' => ['required', 'image'],
            'facebook_link' => ['nullable', 'url'],
            'linkedin_link' => ['nullable', 'url'],
            'youtube_link' => ['nullable', 'url'],
            'twitter_link' => ['nullable', 'url'],
            'email' => ['required', 'email'],
        ]);

        $data['logo'] = uploadFile($request->file('logo'));

        SiteSetting::create($data);

        $request->session()->flash('message', 'Site settings created successfully');
        $request->session()->flash('alert-class', 'alert alert-success');
        return redirect()->back();
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $settings = SiteSetting::first();

        $data = $request->validate([
            'logo' => ['nullable', 'image'],
            'facebook_link' => ['nullable', 'url'],
            'linkedin_link' => ['nullable', 'url'],
            'youtube_link' => ['nullable', 'url'],
            'twitter_link' => ['nullable', 'url'],
            'email' => ['required', 'email'],
        ]);

        if ($request->logo) {
            $data['logo'] = uploadFile($request->file('logo'));
        } else {
            $data['logo'] = $settings->logo;
        }

        $settings->update($data);


        $request->session()->flash('message', 'Site settings updated successfully');
        $request->session()->flash('alert-class', 'alert alert-success');
        return redirect()->back();
    }
}
