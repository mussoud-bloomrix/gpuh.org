<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Mail\ContactMail;
use Illuminate\Http\Request;
use App\Models\Campaign;
use App\Models\CampaignPost;
use App\Models\Initiative;
use App\Models\News;
use App\Models\PressRelease;
use App\Rules\PhoneNumber;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;

class PageController extends Controller
{
    public function index()
    {
        $initiatives = Initiative::oldest()->get();
        $campaigns = Campaign::oldest()->get();
        $pressRelease = PressRelease::latest()->first();
        $inTheNews = News::where('type', 'In the news')->latest()->first();
        $latestNews = News::where('type', 'Latest news')->latest()->first();
        return view('frontend.home', compact('initiatives', 'campaigns', 'pressRelease', 'inTheNews', 'latestNews'));
    }

    public function initiatives()
    {
        $initiatives = Initiative::latest()->get();
        return view('frontend.pages.initiatives', compact('initiatives'));
    }
    public function initiative(Initiative $initiative)
    {
        $campaigns = $initiative->campaigns()->latest()->get();
        return view('frontend.pages.initiative', compact('campaigns', 'initiative'));
    }
    public function campaigns()
    {
        $campaigns = Campaign::latest()->get();
        return view('frontend.pages.campaigns', compact('campaigns'));
    }
    public function campaign(Initiative $initiative, Campaign $campaign)
    {
        $mediaList = $campaign->posts()->latest()->get();
        return view('frontend.pages.campaign', compact('initiative', 'campaign', 'mediaList'));
    }

    public function mediaCenterDetail(Initiative $initiative, Campaign $campaign, $mediaSlug)
    {
        $media = News::where('slug', $mediaSlug)->first();
        $recentPosts = News::where('slug', '!=', $mediaSlug)->latest()->take(5)->get();
        if (!$media) {
            $media = PressRelease::where('slug', $mediaSlug)->first();
            $recentPosts = PressRelease::where('slug', '!=', $mediaSlug)->latest()->take(5)->get();
        }
        $relatedPosts = $campaign->posts()->wherehas('post', function ($qurey) use ($mediaSlug) {
            $qurey->where('slug', '!=', $mediaSlug);
        })->take(8)->get();
        return view('frontend.pages.media-center-detail', compact('initiative', 'campaign', 'media', 'relatedPosts', 'recentPosts'));
    }
    public function inTheNewsListing()
    {
        $inTheNews = News::where('type', 'In the news')->latest()->get();
        return view('frontend.pages.in-the-news', compact('inTheNews'));
    }
    public function latestNewsListing()
    {
        $latestNews = News::where('type', 'Latest news')->latest()->get();
        return view('frontend.pages.latest-news', compact('latestNews'));
    }
    public function pressReleases()
    {
        $pressReleases = PressRelease::latest()->get();
        return view('frontend.pages.press-releases', compact('pressReleases'));
    }
    public function pressRelease(PressRelease $media)
    {
        $mediaSlug = $media->slug;
        if (count($media->campaigns) > 0) {
            $campaign = $media->campaigns[0];
            $initiative = $campaign->initiative;
            $relatedPosts = $campaign->posts()->wherehas('post', function ($qurey) use ($mediaSlug) {
                $qurey->where('slug', '!=', $mediaSlug);
            })->take(8)->get();
        } else {
            $campaign = null;
            $initiative = null;
            $relatedPosts = null;
        }
        $breadcrumb = 'press-releases';
        $recentPosts = PressRelease::where('slug', '!=', $mediaSlug)->latest()->take(5)->get();
        return view('frontend.pages.media-center-detail', compact('media', 'campaign', 'initiative', 'relatedPosts', 'recentPosts', 'breadcrumb'));
    }
    public function inTheNews(News $media)
    {
        $mediaSlug = $media->slug;
        if (count($media->campaigns) > 0) {
            $campaign = $media->campaigns[0];
            $initiative = $campaign->initiative;
            $relatedPosts = $campaign->posts()->wherehas('post', function ($qurey) use ($mediaSlug) {
                $qurey->where('slug', '!=', $mediaSlug);
            })->take(8)->get();
        } else {
            $campaign = null;
            $initiative = null;
            $relatedPosts = null;
        }
        $breadcrumb = 'in-the-news';
        $recentPosts = News::where('slug', '!=', $mediaSlug)->latest()->take(5)->get();
        return view('frontend.pages.media-center-detail', compact('media', 'campaign', 'initiative', 'relatedPosts', 'recentPosts', 'breadcrumb'));
    }
    public function latestNews(News $media)
    {
        $mediaSlug = $media->slug;
        if (count($media->campaigns) > 0) {
            $campaign = $media->campaigns[0];
            $initiative = $campaign->initiative;
            $relatedPosts = $campaign->posts()->wherehas('post', function ($qurey) use ($mediaSlug) {
                $qurey->where('slug', '!=', $mediaSlug);
            })->take(8)->get();
        } else {
            $campaign = null;
            $initiative = null;
            $relatedPosts = null;
        }
        $breadcrumb = 'latest-news';
        $recentPosts = News::where('slug', '!=', $mediaSlug)->latest()->take(5)->get();

        return view('frontend.pages.media-center-detail', compact('media', 'campaign', 'initiative', 'relatedPosts', 'recentPosts', 'breadcrumb'));
    }
    public function whoWeAre()
    {
        return view('frontend.pages.who-we-are');
    }
    public function whatWeDo()
    {
        return view('frontend.pages.what-we-do');
    }
    public function privacyPolicy()
    {
        return view('frontend.pages.privacy-policy');
    }

    public function cookiePolicy()
    {
        return view('frontend.pages.cookie-policy');
    }

    public function termsOfUse()
    {
        return view('frontend.pages.terms-of-use');
    }
    public function contact()
    {
        return view('frontend.pages.contact');
    }
    public function contactMail(Request $request)
    {
        $data = $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'email'],
            'phone' => ['required'],
            'message' => ['nullable'],
        ]);
        Mail::to(config('mail.to.address'))->send(new ContactMail($data));
        return redirect()->route('pages.contact', '#contact-form')->with('success', ' "Thanks you for contacting us – We will get back to you soon" ');
    }
}
