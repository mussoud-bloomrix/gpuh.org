<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Campaign extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function initiative()
    {
        return $this->belongsTo(Initiative::class);
    }

    /**
     * Get all of the posts for the Campaign
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function posts(): HasMany
    {
        return $this->hasMany(CampaignPost::class)->with('post');
    }

    /**
     * Get all of the posts that are assigned this tag.
     */
    public function pressReleases()
    {
        return $this->morphedByMany(PressRelease::class, 'postable', 'campaign_posts');
    }

    /**
     * Get all of the posts that are assigned this tag.
     */
    public function news()
    {
        return $this->morphedByMany(News::class, 'postable', 'campaign_posts');
    }
}
