<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CampaignPost extends Model
{
    use HasFactory;

    protected $guarded = [];

    /**
     * Get all of the posts that are assigned this tag.
     */
    public function post()
    {
        return $this->morphTo('postable');
    }
}
