<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PressRelease extends Model
{
    use HasFactory;

    protected $with = ['campaigns'];

    protected $guarded = [];

    protected $casts = [
        'date' => 'date',
        // 'slider_images' => 'array'
    ];

    /**
     * Get all of the campaigns for the press release.
     */
    public function campaigns()
    {
        return $this->morphToMany(Campaign::class, 'postable', 'campaign_posts');
    }

    public function sliderImages()
    {
        return json_decode($this->slider_images);
    }
}
