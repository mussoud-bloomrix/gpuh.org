<?php

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

if (!function_exists('uploadFile')) {
    function uploadFile($file)
    {
        $filename = Str::random(15) . '.' . $file->getClientOriginalExtension();
        Storage::putFileAs('public', $file, $filename);
        return $filename;
    }
}
