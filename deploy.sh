# Change to the project directory
echo "Change directory: staging.gpuh.org"
cd ~/staging.gpuh.org

# Turn on maintenance mode
echo "Turning on maintenance mode"
/usr/bin/php73-cli artisan down

# Pull the latest changes from the git repository
echo "Pulling latest changes from git repo"
git fetch --all
git reset --hard origin/develop
git checkout develop
git pull origin develop

# Install/update composer dependecies
echo "Installing composer dependencies"
/usr/bin/php73-cli ~/composer.phar install --optimize-autoloader --no-dev
/usr/bin/php73-cli ~/composer.phar dump-autoload
# Run database migrations
echo "Running database migrations"
/usr/bin/php73-cli artisan migrate

echo "Recaching"
/usr/bin/php73-cli artisan config:cache
/usr/bin/php73-cli artisan route:cache
/usr/bin/php73-cli artisan view:cache

# Install node modules
# npm install

# Build assets using Laravel Mix
# npm run production

cd ~/public_html/staging
find . -type f -not -name 'temp.php' -not -name 'storage' -not -name '.htaccess' -delete
cp -R ~/staging.gpuh.org/public/* .
rm -rf index.php
cp temp.php index.php

echo "Changing directory back to: staging.gpuh.org"
cd ~/staging.gpuh.org

# Turn off maintenance mode
echo "Turning off maintenance mode"
/usr/bin/php73-cli artisan up

echo 'Deploy finished.'
