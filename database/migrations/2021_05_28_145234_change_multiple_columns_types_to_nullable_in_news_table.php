<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeMultipleColumnsTypesToNullableInNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('news', function (Blueprint $table) {
            $table->date('date')->nullable()->change();
            $table->text('excerpt')->nullable()->change();
            $table->text('introduction')->nullable()->change();
            $table->text('body')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('news', function (Blueprint $table) {
            $table->date('date')->nullable(false)->change();
            $table->text('excerpt')->nullable(false)->change();
            $table->text('introduction')->nullable(false)->change();
            $table->text('body')->nullable(false)->change();
        });
    }
}
