<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class MakeTaglineNullableInInitiativesAndCampaignsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('initiatives', function (Blueprint $table) {
            $table->string('tag_line')->nullable()->change();
        });
        Schema::table('campaigns', function (Blueprint $table) {
            $table->string('tag_line')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('initiatives', function (Blueprint $table) {
            $table->string('tag_line')->nullable(false)->change();
        });
        Schema::table('campaigns', function (Blueprint $table) {
            $table->string('tag_line')->nullable(false)->change();
        });
    }
}
