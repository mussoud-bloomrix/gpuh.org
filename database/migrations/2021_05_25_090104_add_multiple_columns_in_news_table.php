<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMultipleColumnsInNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('news', function (Blueprint $table) {
            $table->string('tagline')->after('title');
            $table->string('video_url')->nullable();
            $table->string('image')->nullable();
            $table->text('introduction');
            $table->text('slider_images')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('news', function (Blueprint $table) {
            $table->drop('tagline');
            $table->drop('video_url');
            $table->drop('image');
            $table->drop('introduction');
            $table->drop('slider_images');
        });
    }
}
