@if (Session('success'))
<div class="alert alert-success alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
            aria-hidden="true">&times;</span></button>
    {!!Session('success')!!}
</div>
@endif

<div id="dp-block" style="display: none;" class="alert alert-success alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
            aria-hidden="true">&times;</span></button>
    Add to favorite list
</div>

<div id="dp-block-danger" style="display: none;" class="alert alert-danger alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
            aria-hidden="true">&times;</span></button>
    Instructor is already in favorite list
</div>

@if (Session('warning'))
<div class="alert alert-warning alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
            aria-hidden="true">&times;</span></button>
    {!!Session('warning')!!}
</div>
@endif

@if (Session('danger'))
<div class="alert alert-danger alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
            aria-hidden="true">&times;</span></button>
    {!!Session('danger')!!}
</div>
@endif

@if ($errors->any())
<div class="alert alert-danger alert-dismissible" role='alert'>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
            aria-hidden="true">&times;</span></button>
    Please check form errors before trying to submit again
</div>
@endif

@if (session('message'))
<div id="any-message-for-anything" class="alert {{ session('alert-class', 'alert-info') }} border-0 alert-dismissible">
    <button type="button" class="close" data-dismiss="alert"><span>&times;</span></button>
    {!! session('message') !!}
</div>
@endif
