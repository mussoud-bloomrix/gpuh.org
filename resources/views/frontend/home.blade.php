@extends('frontend.layouts.layout')
@section('head')
<title>Global Peace & Unity for Humanity</title>
<meta name="description" content="GPUH">
<meta name="keywords" content="GPUH">
@endsection

@section('content')


<!--=================================
        banner -->
<section class="rev-slider">
  <div id="rev_slider_267_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-alias="webster-slider-1" data-source="gallery" style="margin:0px auto;background:transparent;padding:0px;margin-top:0px;margin-bottom:0px;">
    <!-- START REVOLUTION SLIDER 5.4.6.3 fullwidth mode -->
    <div id="rev_slider_267_1" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.4.6.3">
      <ul>
        <!-- SLIDE  -->
        <li data-index="rs-755" data-transition="random-static,random-premium,random" data-slotamount="default,default,default,default" data-hideafterloop="0" data-hideslideonmobile="off" data-randomtransition="on" data-easein="default,default,default,default" data-easeout="default,default,default,default" data-masterspeed="default,default,default,default" data-thumb="{{ asset('frontend/images/slider-1.jpg') }}" data-rotate="0,0,0,0" data-saveperformance="off" data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
          <!-- MAIN IMAGE -->
          <img src="{{ asset('frontend/images/slider-1.jpg') }}" alt="" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
          <!-- LAYERS -->
          <!-- LAYER NR. 9 -->
          <div class="tp-caption   tp-resizeme" id="slide-756-layer-2" data-x="60" data-y="290" data-width="['auto']" data-height="['auto']" data-type="text" data-responsive_offset="on" data-frames='[{"delay":1000,"speed":1500,"frame":"0","from":"y:-50px;opacity:0;","to":"o:1;","ease":"Power2.easeOut"},{"delay":"wait","speed":1000,"frame":"999","to":"opacity:0;","ease":"nothing"}]' data-textAlign="['left','left','left','left']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 5; white-space: nowrap; font-size: 60px; line-height: 70px; font-weight: 300; color: rgba(255,255,255,1); font-family:Montserrat ;">
            Making the world
          </div>
          <!-- LAYER NR. 15 -->
          <div class="tp-caption   tp-resizeme" id="slide-756-layer-15" data-x="60" data-y="360" data-voffset="6" data-width="['auto']" data-height="['auto']" data-type="text" data-responsive_offset="on" data-frames='[{"delay":1080,"speed":1500,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power2.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"opacity:0;","ease":"nothing"}]' data-textAlign="['left','left','left','left']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 11; white-space: nowrap; font-size: 60px; line-height: 70px; font-weight: 400; color: rgba(255,255,255,1); font-family:Montserrat ;">
            a better place to live
          </div>
          <!-- LAYER NR. 12 -->
          <div class="tp-caption   tp-resizeme" id="slide-756-layer-3" data-x="60" data-y="440" data-voffset="85" data-width="['auto']" data-height="['auto']" data-type="text" data-responsive_offset="on" data-frames='[{"delay":2090,"speed":1500,"frame":"0","from":"x:left;","to":"o:1;","ease":"Power3.easeOut"},{"delay":"wait","speed":1000,"frame":"999","to":"opacity:0;","ease":"nothing"}]' data-textAlign="['left','left','left','left']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 8; white-space: nowrap; font-size: 24px; line-height: 38px; font-weight: 200; color: rgba(255,255,255,1); font-family: Montserrat ;">
            GPUH is helping individuals, communities and countries navigate the
            <br>difficult path from conflict to peace and unity.

          </div>
        </li>
        <!-- SLIDE  -->
        <li data-index="rs-756" data-transition="random-static,random-premium,random" data-slotamount="default,default,default,default" data-hideafterloop="0" data-hideslideonmobile="off" data-randomtransition="on" data-easein="default,default,default,default" data-easeout="default,default,default,default" data-masterspeed="default,default,default,default" data-thumb="{{ asset('frontend/images/peace.jpg') }}" data-rotate="0,0,0,0" data-saveperformance="off" data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
          <!-- MAIN IMAGE -->
          <img src="{{ asset('frontend/images/peace.jpg') }}" alt="" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
          <!-- LAYERS -->
          <!-- LAYER NR. 9 -->
          <div class="tp-caption   tp-resizeme" id="slide-756-layer-2" data-x="60" data-y="290" data-width="['auto']" data-height="['auto']" data-type="text" data-responsive_offset="on" data-frames='[{"delay":1000,"speed":1500,"frame":"0","from":"y:-50px;opacity:0;","to":"o:1;","ease":"Power2.easeOut"},{"delay":"wait","speed":1000,"frame":"999","to":"opacity:0;","ease":"nothing"}]' data-textAlign="['left','left','left','left']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 5; white-space: nowrap; font-size: 60px; line-height: 70px; font-weight: 300; color: rgba(255,255,255,1); font-family:Montserrat ;">
            Promoting peace through
          </div>
          <!-- LAYER NR. 15 -->
          <div class="tp-caption   tp-resizeme" id="slide-756-layer-15" data-x="60" data-y="360" data-voffset="6" data-width="['auto']" data-height="['auto']" data-type="text" data-responsive_offset="on" data-frames='[{"delay":1080,"speed":1500,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power2.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"opacity:0;","ease":"nothing"}]' data-textAlign="['left','left','left','left']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 11; white-space: nowrap; font-size: 60px; line-height: 70px; font-weight: 400; color: rgba(255,255,255,1); font-family:Montserrat ;">
            intermediation & dialogues
          </div>
          <!-- LAYER NR. 12 -->
          <div class="tp-caption   tp-resizeme" id="slide-756-layer-3" data-x="60" data-y="440" data-voffset="85" data-width="['auto']" data-height="['auto']" data-type="text" data-responsive_offset="on" data-frames='[{"delay":2090,"speed":1500,"frame":"0","from":"x:left;","to":"o:1;","ease":"Power3.easeOut"},{"delay":"wait","speed":1000,"frame":"999","to":"opacity:0;","ease":"nothing"}]' data-textAlign="['left','left','left','left']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 8; white-space: nowrap; font-size: 24px; line-height: 38px; font-weight: 200; color: rgba(255,255,255,1); font-family: Montserrat ;">
            GPUH aim is to promote peace by raising awareness in the legal context,
            <br>provide intermediary role to resolve conflicts.
          </div>
        </li>
        <li data-index="rs-757" data-transition="random-static,random-premium,random" data-slotamount="default,default,default,default" data-hideafterloop="0" data-hideslideonmobile="off" data-randomtransition="on" data-easein="default,default,default,default" data-easeout="default,default,default,default" data-masterspeed="default,default,default,default" data-thumb="{{ asset('frontend/images/unity.jpg') }}" data-rotate="0,0,0,0" data-saveperformance="off" data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
          <!-- MAIN IMAGE -->
          <img src="{{ asset('frontend/images/unity.jpg') }}" alt="" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
          <!-- LAYERS -->
          <!-- LAYER NR. 9 -->
          <div class="tp-caption   tp-resizeme" id="slide-756-layer-2" data-x="60" data-y="290" data-width="['auto']" data-height="['auto']" data-type="text" data-responsive_offset="on" data-frames='[{"delay":1000,"speed":1500,"frame":"0","from":"y:-50px;opacity:0;","to":"o:1;","ease":"Power2.easeOut"},{"delay":"wait","speed":1000,"frame":"999","to":"opacity:0;","ease":"nothing"}]' data-textAlign="['left','left','left','left']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 5; white-space: nowrap; font-size: 60px; line-height: 70px; font-weight: 300; color: rgba(255,255,255,1); font-family:Montserrat ;">
            Uniting millions of people
          </div>
          <div class="tp-caption   tp-resizeme" id="slide-756-layer-15" data-x="60" data-y="360" data-voffset="6" data-width="['auto']" data-height="['auto']" data-type="text" data-responsive_offset="on" data-frames='[{"delay":1080,"speed":1500,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power2.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"opacity:0;","ease":"nothing"}]' data-textAlign="['left','left','left','left']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 11; white-space: nowrap; font-size: 60px; line-height: 70px; font-weight: 400; color: rgba(255,255,255,1); font-family:Montserrat ;">
            across the globe
          </div>
          <!-- LAYER NR. 12 -->
          <div class="tp-caption   tp-resizeme" id="slide-756-layer-3" data-x="60" data-y="440" data-voffset="85" data-width="['auto']" data-height="['auto']" data-type="text" data-responsive_offset="on" data-frames='[{"delay":2090,"speed":1500,"frame":"0","from":"x:left;","to":"o:1;","ease":"Power3.easeOut"},{"delay":"wait","speed":1000,"frame":"999","to":"opacity:0;","ease":"nothing"}]' data-textAlign="['left','left','left','left']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 8; white-space: nowrap; font-size: 24px; line-height: 38px; font-weight: 200; color: rgba(255,255,255,1); font-family: Montserrat ;">
            GPUH belives in strength of the unity and we help unite people from
            <br>across different regions, cultures and religions.
          </div>
        </li>
      </ul>
      <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>
    </div>
  </div>
</section>
<!--=================================
        banner -->

<!--=================================
        GPUH Mission Statement-->
<section class="page-section-ptb" id="WhoWeAre">
  <div class="container">
    <div class="row">
      <div class="col-lg-4 col-md-4 col-sm-4">
        <div class="feature-text left-icon mt-60 xs-mt-20">
          <div class="feature-icon theme-color">
            <img src="{{ asset('frontend/icons/mission.png') }}" alt="" class="" height="40px" width="40px">
          </div>
          <div class="feature-info">
            <h5 class="text-back">GPUH Mission Statement</h5>
            <p class="gpuh-intro">To make world a peaceful place to live</p>
          </div>
        </div>
      </div>
      <div class="col-lg-4 col-md-4 col-sm-4">
        <div class="feature-text left-icon mt-60 xs-mt-20">
          <div class="feature-icon theme-color">
            <img src="{{ asset('frontend/icons/vision.png') }}" alt="" class="" height="40px" width="40px">
          </div>
          <div class="feature-info">
            <h5 class="text-back">GPUH Vision Statement</h5>
            <p class="gpuh-intro">A peaceful <br> and united <br> human race</p>
          </div>
        </div>
      </div>
      <div class="col-lg-4 col-md-4 col-sm-4">
        <div class="feature-text left-icon mt-60 xs-mt-20">
          <div class="feature-icon theme-color">
            <img src="{{ asset('frontend/icons/ethos.png') }}" alt="" class="" height="40px" width="40px">
          </div>
          <div class="feature-info">
            <h5 class="text-bac">GPUH Ethos</h5>
            <p class="gpuh-intro">To do everything in the light of international laws</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!--=================================
        End GPUH Mission Statement-->

<!--=================================
        WhoWeAre-->
<section class="page-section-ptb gray-bg">
  <div class="container">
    <div class="row">
      <div class="col-lg-6">
        <img src="{{ asset('frontend/images/who-we-are.jpg') }}" class="img-fluid full-width" alt="">
      </div>
      <div class="col-lg-6 sm-mt-30">
        <div class="section-title">
          <h2>Who We Are</h2>
        </div>
        <p class="text-custom">As our name explains, Global Peace & Unity for Humanity is about a Peaceful and a United world. We want to bring more peace and unity into this world. Whether it’s between individuals, among communities or nations, we are committed and driven to promote peace by supporting the compassionate understanding of others, respectful assertion of needs, and collaborative dialogue in conflict.</p>
      </div>
    </div>
  </div>
</section>
<!--=================================
        End WhoWeAre-->

<!--=================================
        WhatWeDo-->
<section class="page-section-ptb" id="WhatWeDo">
  <div class="container">
    <div class="row">
      <div class="col-lg-6 sm-mt-30">
        <div class="section-title">
          <h2 class="">What We Do</h2>
        </div>
        <p class="text-custom">We plan to achieve our vision and mission by raising awareness about the matters
          that cause conflict among individuals and communities. We do all what we do in the light of national
          & international law. We intend to raise this awareness by arranging educational seminars, virtual
          events, campaigns and promotions on various Digital and Print media.</p>
      </div>
      <div class="col-lg-6">
        <img src="{{ asset('frontend/images/what-we-do.jpg') }}" class="img-fluid full-width" alt="">
      </div>
    </div>
  </div>
</section>
<!--=================================
        End WhatWeDo-->

<!--=================================
 Initiative-->
 @if($initiatives)
<section class="page-section-pt">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 col-md-12 mt-30">
        <div class="section-title line center text-center">
          <h2 class="title">Our Initiatives</h2>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="blog blog-grid-3-column white-bg page-section-ptb">
  <div class="container">
    <div class="row">
        @foreach($initiatives->take(3) as $initiative)
        <div class="col-lg-4 col-md-4">
            <div class="blog-entry mb-50">
                <div class="entry-image clearfix">
                    <a href="{{route('pages.initiative', $initiative->slug)}}">
                        <img class="img-fluid" src="{{ asset( asset('storage/' . $initiative->feature_image)) }}" alt="">
                    </a>
                </div>
                <div class="blog-detail">
                    <div class="entry-title mb-10">
                        <a href="{{route('pages.initiative', $initiative->slug)}}">{{ $initiative->name }}</a>
                    </div>
                    <div class="entry-content">
                        <div>{{$initiative->excerpt}} </div>
                    </div>
                    <div class="entry-share clearfix">
                        <div class="entry-button">
                            <a class="button arrow" href="{{route('pages.initiative', $initiative->slug)}}">Read More<i class="fa fa-angle-right" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    </div>
    @if(count($initiatives) > 3)
        <div class="text-center">
        <a class="button medium" href="{{route('pages.initiatives')}}">Explore All Initiatives</a>
        </div>
    @endif
  </div>
</section>
@endif
<!--=================================
 Campaigns-->
 @if($campaigns)
<section class="page-section-pt">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 col-md-12 mt-30">
        <div class="section-title line center text-center">
          <h2 class="title">Our Campaigns</h2>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="blog blog-grid-3-column white-bg page-section-ptb">
  <div class="container">
    <div class="row">
        @foreach ($campaigns->take(3) as $campaign)
        <div class="col-lg-4 col-md-4">
            <div class="blog-entry mb-50">
              <div class="entry-image clearfix">
                <a href="{{route('pages.campaign',[$campaign->initiative->slug, $campaign->slug]) }}">
                    <img class="img-fluid" src="{{ asset('storage/' . $campaign->feature_image) }}" alt="">
                </a>

              </div>
              <div class="blog-detail">
                <div class="entry-title mb-10">
                    <a href="{{route('pages.campaign',[$campaign->initiative->slug, $campaign->slug]) }}">{{ $campaign->name}}</a>
                </div>
                <div class="entry-meta mb-10">
                  <ul>
                    <li> <i class="fa fa-folder-open-o"></i> <a href="#"> {{$campaign->initiative->name }}</a> </li>
                    <li><a href="#"><i class="fa fa-calendar-o"></i> {{ \Carbon\Carbon::parse($campaign->created_at)->format('M d, Y') }}</a></li>
                  </ul>
                </div>
                <div class="entry-content">
                    <p>{!! $campaign->excerpt !!}</p>
                </div>
                <div class="entry-share clearfix">
                    <div class="entry-button">
                        <a class="button arrow" href="{{route('pages.campaign',[$campaign->initiative->slug, $campaign->slug]) }}">Read More<i class="fa fa-angle-right" aria-hidden="true"></i></a>
                    </div>
                    <div class="social list-style-none float-right">
                        <strong>Share : </strong>
                        <ul>
                            <li>
                                <a href="#"> <i class="fa fa-facebook"></i> </a>
                            </li>
                            <li>
                                <a href="#"> <i class="fa fa-twitter"></i> </a>
                            </li>
                            <li>
                                <a href="#"> <i class="fa fa-instagram"></i> </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endforeach
</div>
@if(count($campaigns) > 3)
<div class="text-center">
    <a class="button medium" href="{{route('pages.campaigns')}}">Explore All Campaigns</a>
</div>
@endif
  </div>
</section>
@endif
<!--=================================
 Media Center-->
<section class="page-section-pt">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 col-md-12 mt-30">
        <div class="section-title line center text-center">
          <h2 class="title">Media Center</h2>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="blog blog-grid-3-column white-bg page-section-ptb">
  <div class="container">
    <div class="row">
        {{-- @if ($pressRelease)
        <div class="col-lg-4 col-md-4">
            <div class="blog-entry mb-50">
              <div class="entry-image clearfix">
                <img class="img-fluid" src="{{ asset('storage/' . $pressRelease->featured_image) }}" alt="">
              </div>
              <div class="blog-detail">
                <div class="entry-title mb-10">
                    <a href="{{route('pages.press-release', $pressRelease->slug )}}">{{ $pressRelease->title }}</a>
                </div>
                <div class="entry-meta mb-10">
                  <ul>
                    <li> <i class="fa fa-folder-open-o"></i>
                        @foreach($pressRelease->campaigns as $key => $campaign)
                        <a href="{{route('pages.campaign', [$campaign->initiative->slug, $campaign->slug])}}"> {{ $campaign->name }} @if(count($pressRelease->campaigns) != $key+1), @endif
                        </a>
                        @endforeach
                    </li>
                    <li><a href="#"><i class="fa fa-calendar-o"></i> {{ \Carbon\Carbon::parse($pressRelease->created_at)->format('M d, Y') }}</a></li>
                  </ul>
                </div>
                <div class="entry-content">
                    <p>{!! $pressRelease->excerpt !!}</p>
                </div>
                <div class="entry-share clearfix">
                  <div class="entry-button">
                    <a class="button arrow" href="{{route('pages.press-release', $pressRelease->slug )}}">Read More<i class="fa fa-angle-right" aria-hidden="true"></i></a>
                  </div>
                  <div class="social list-style-none float-right">
                    <strong>Share : </strong>
                    <ul>
                      <li>
                        <a href="#"> <i class="fa fa-facebook"></i> </a>
                      </li>
                      <li>
                        <a href="#"> <i class="fa fa-twitter"></i> </a>
                      </li>
                      <li>
                        <a href="#"> <i class="fa fa-instagram"></i> </a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        @endif --}}
      {{-- @if($inTheNews)
      <div class="col-lg-4 col-md-4">
        <div class="blog-entry mb-50">
          <div class="entry-image clearfix">
            <img class="img-fluid" src="{{ asset('storage/' . $inTheNews->featured_image) }}" alt="">
          </div>
          <div class="blog-detail">
            <div class="entry-title mb-10">
              <a href="{{route('pages.in-the-news-detail', $inTheNews->slug )}}">{{ $inTheNews->title }}</a>
            </div>
            <div class="entry-meta mb-10">
              <ul>
                <li> <i class="fa fa-folder-open-o"></i>
                  @foreach($inTheNews->campaigns as $key => $campaign)
                  <a href="{{route('pages.campaign', [$campaign->initiative->slug, $campaign->slug])}}"> {{ $campaign->name }} @if(count($inTheNews->campaigns) != $key+1), @endif
                  </a>
                  @endforeach
              </li>
                <li><a href="#"><i class="fa fa-calendar-o"></i> {{ \Carbon\Carbon::parse($inTheNews->created_at)->format('M d, Y') }}</a></li>
              </ul>
            </div>
            <div class="entry-content">
              <p>{!! $inTheNews->excerpt !!}</p>
            </div>
            <div class="entry-share clearfix">
              <div class="entry-button">
                <a class="button arrow" href="{{route('pages.in-the-news-detail', $inTheNews->slug )}}">Read More<i class="fa fa-angle-right" aria-hidden="true"></i></a>
              </div>
              <div class="social list-style-none float-right">
                <strong>Share : </strong>
                <ul>
                  <li>
                    <a href="#"> <i class="fa fa-facebook"></i> </a>
                  </li>
                  <li>
                    <a href="#"> <i class="fa fa-twitter"></i> </a>
                  </li>
                  <li>
                    <a href="#"> <i class="fa fa-instagram"></i> </a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
      @endif --}}
      @if ($latestNews)
      <div class="col-lg-4 col-md-4">
        <div class="blog-entry mb-50">
          <div class="entry-image clearfix">
            <a href="{{route('pages.latest-news-detail', $latestNews->slug )}}">
                <img class="img-fluid" src="{{ asset('storage/' . $latestNews->featured_image) }}" alt="">
            </a>

          </div>
          <div class="blog-detail">
            <div class="entry-title mb-10">
              <a href="{{route('pages.latest-news-detail', $latestNews->slug )}}">{{ $latestNews->title }}</a>
            </div>
            <div class="entry-meta mb-10">
              <ul>
                <li> <i class="fa fa-folder-open-o"></i>
                  @foreach($latestNews->campaigns as $key => $campaign)
                  <a href="{{route('pages.campaign', [$campaign->initiative->slug, $campaign->slug])}}"> {{ $campaign->name }} @if(count($latestNews->campaigns) != $key+1), @endif
                  </a>
                  @endforeach
              </li>
                <li><a href="#"><i class="fa fa-calendar-o"></i> {{ \Carbon\Carbon::parse($latestNews->created_at)->format('M d, Y') }}</a></li>
              </ul>
            </div>
            <div class="entry-content">
              <p>{!! $latestNews->excerpt !!}</p>
            </div>
            <div class="entry-share clearfix">
              <div class="entry-button">
                <a class="button arrow" href="{{route('pages.latest-news-detail', $latestNews->slug )}}">Read More<i class="fa fa-angle-right" aria-hidden="true"></i></a>
              </div>
              <div class="social list-style-none float-right">
                <strong>Share : </strong>
                <ul>
                  <li>
                    <a href="#"> <i class="fa fa-facebook"></i> </a>
                  </li>
                  <li>
                    <a href="#"> <i class="fa fa-twitter"></i> </a>
                  </li>
                  <li>
                    <a href="#"> <i class="fa fa-instagram"></i> </a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
      @endif
    </div>
  </div>
</section>

</div>
<!-- wrapper End -->


@endsection
