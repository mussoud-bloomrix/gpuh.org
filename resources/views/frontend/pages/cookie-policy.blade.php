@extends('frontend.layouts.layout')

@section('head')
<title>Global Peace & Unity for Humanity</title>
<meta name="description" content="GPUH">
<meta name="keywords" content="GPUH">
@endsection
@section('breadcrumbs')
<ul class="page-breadcrumb">
    <li><a href="#"><i class="fa fa-home"></i> Home</a> <i class="fa fa-angle-double-right"></i></li>
    <li><a href="#">Cookie Policy</a></li>
  </ul>
  @endsection
@section('content')
@include('frontend.partials.header',
['title'=> 'Cookie Policy', 'subtitle'=>'', 'prefix' => 'frontend/images/', 'image'=> 'cookie-policy.jpg'
])
<div class="container my-5 mt-80 mb-80">
    <script id="CookieDeclaration" src="https://consent.cookiebot.com/014fab7a-3a2e-4a67-9754-6089c42d281c/cd.js" type="text/javascript" async></script>
</div>
@endsection
