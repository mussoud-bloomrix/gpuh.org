@extends('frontend.layouts.layout')

@section('head')
<title>Global Peace & Unity for Humanity</title>
<meta name="description" content="GPUH">
<meta name="keywords" content="GPUH">
@endsection
@section('breadcrumbs')
<ul class="page-breadcrumb">
    <li><a href="#"><i class="fa fa-home"></i> Home</a> <i class="fa fa-angle-double-right"></i></li>
    <li><a href="#">Privacy Policy</a></li>
  </ul>
  @endsection
@section('content')
@include('frontend.partials.header',
['title'=> 'Privacy Policy', 'subtitle'=>'','prefix' => 'frontend/images/', 'image'=> 'privacy-policy.jpg'
])
<div class="container my-5 mt-80 mb-80">
    <p>GPUH is committed to protect its clients and website visitor&rsquo;s privacy.</p>
    <p></p>
    <p>We will ensure that all sensitive and personal information provided to us is lawfully processed and held in accordance to the Data Protection Act 1998.</p>
    <p></p>
    <p>How do we collect information?</p>
    <p></p>
    <p>The information is collected when you make an enquiry online from our website, call us or send us an email. We collect and keep following information on our website:</p>
    <p>- Your full name</p>
    <p>- Your valid email address</p>
    <p>- Your telephone / mobile number</p>
    <p></p>
    <p>What do we do with the information collected?</p>
    <p></p>
    <p>We will only make use of the information you provided us in following ways:</p>
    <p>- Respond to your requests and enquiries</p>
    <p>- Suggestion and improvements to our services</p>
    <p>- Provide information to you held by us</p>
    <p>- Forward you the marketing emails with your consent</p>
    <p></p>
    <p>We will not send any e-mails to you in the future unless you have given us permission to do so. We will provide you the opportunity to refuse any e-mails from us or from a 3rd party in the future.</p>
    <p></p>
    <p>We do not sell, rent or exchange your personal information with any third party for commercial reasons.</p>
    <p></p>
    <p>We follow strict security procedures in the storage and disclosure of information which you have given us, to prevent unauthorised access in accordance with the UK data protection legislation.</p>
    <p></p>
    <p>If you need to find out what information we hold about you, you can request us in writing to our postal address</p>
    <p></p>
    <p class="mb-0">Kemp House</p>
    <p class="my-0">152 - 160 City Road</p>
    <p class="my-0">London</p>
    <p class="my-0">EC1V 2NX</p>
    <p></p>
    <p>If any inaccuracies are found in the data held by us, we will make the appropriate changes or permanently delete it.</p>
    <p></p>
    <p>We will not transfer your information outside the EEA (European Economic Area) without first obtaining your consent.</p>
    <p></p>
    <p>If you have any questions/comments about Privacy, you should e-mail us at info@gpuh.org.</p>
</div>
@endsection
