@extends('frontend.layouts.layout')

@section('head')
<title>Global Peace & Unity for Humanity</title>
<meta name="description" content="GPUH">
<meta name="keywords" content="GPUH">
@endsection
@section('breadcrumbs')
<ul class="page-breadcrumb">
    <li><a href="#"><i class="fa fa-home"></i> Home</a> <i class="fa fa-angle-double-right"></i></li>
    <li><a href="{{route('pages.campaigns')}}">Campaign</a></li>
  </ul>
  @endsection
@section('content')
@include('frontend.partials.header',
['title'=> 'Campaigns', 'subtitle'=>'', 'prefix' => 'frontend/', 'image'=> 'images/campaign-banner.jpg'
])

<section class="blog blog-grid-3-column white-bg page-section-ptb">
  <div class="container">
    <div class="row">
        @forelse ($campaigns as $campaign)
        <div class="col-lg-4 col-md-4">
            <div class="blog-entry mb-50">
              <div class="entry-image clearfix">
                <a href="{{route('pages.campaign',[$campaign->initiative->slug, $campaign->slug]) }}">
                    <img class="img-fluid" src="{{ asset('storage/' . $campaign->feature_image) }}" alt="">
                </a>

              </div>
              <div class="blog-detail">
                <div class="entry-title mb-10">
                    <a href="{{route('pages.campaign',[$campaign->initiative->slug, $campaign->slug]) }}">{{ $campaign->name}}</a>
                </div>
                <div class="entry-meta mb-10">
                  <ul>
                    <li> <i class="fa fa-folder-open-o"></i> <a href="#"> {{$campaign->initiative->name }}</a> </li>
                    <li><a href="#"><i class="fa fa-calendar-o"></i> {{ \Carbon\Carbon::parse($campaign->created_at)->format('M d, Y') }}</a></li>
                  </ul>
                </div>
                <div class="entry-content">
                    <p>{!! $campaign->excerpt !!}</p>
                </div>
                <div class="entry-share clearfix">
                  <div class="entry-button">
                    <a class="button arrow" href="{{route('pages.campaign',[$campaign->initiative->slug, $campaign->slug]) }}">Read More<i class="fa fa-angle-right" aria-hidden="true"></i></a>
                  </div>
                  <div class="social list-style-none float-right">
                    <strong>Share : </strong>
                    <ul>
                      <li>
                        <a href="#"> <i class="fa fa-facebook"></i> </a>
                      </li>
                      <li>
                        <a href="#"> <i class="fa fa-twitter"></i> </a>
                      </li>
                      <li>
                        <a href="#"> <i class="fa fa-instagram"></i> </a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
            @empty
          @endforelse
    </div>
  </div>
</section>
<!-- ================================================ -->

@endsection
