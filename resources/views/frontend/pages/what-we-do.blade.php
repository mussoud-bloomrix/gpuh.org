@extends('frontend.layouts.layout')

@section('head')
<title>Global Peace & Unity for Humanity</title>
<meta name="description" content="GPUH">
<meta name="keywords" content="GPUH">
@endsection

@section('breadcrumbs')
<ul class="page-breadcrumb">
    <li><a href="#"><i class="fa fa-home"></i> Home</a> <i class="fa fa-angle-double-right"></i></li>
    <li><a href="{{route('pages.what-we-do')}}">What We Do</a></li>
  </ul>
  @endsection

@section('content')
@include('frontend.partials.header',
['title'=> 'What We Do?', 'subtitle'=>'', 'prefix' => 'frontend/images/', 'image'=> 'what-we-do-banner.jpg'
])

        <section class="page-section-pt">
            <div class="container">
               <div class="row justify-content-center text-center">
                  <div class="col-lg-10 col-md-10 ">
                     <p class="font-40" style="color: #363636;">Promoting Peace & Unity</p>
                     <h2 class="mt-20 font-30">The opposite of peace is conflict. So what a conflict means and what causes conflicts? Let's talk about it.</h2>
                     <p class="normal mt-30">Conflict is a serious disagreement between people, organizations, or countries with opposing opinions. It is defined as a clash arising out of a difference in thought process, attitudes, understanding, interests, requirements and even sometimes perceptions. A conflict results in heated arguments, physical abuses and definitely loss of peace and harmony.
                     </p>
                     <p>We at GPUH aim to promote and achieve peace by getting involved at the various different stages of peacebuilding </p>
                  </div>
               </div>
            </div>
            <div class="mt-0 text-center py-80 ">
              <img class="img-fluid mx-auto" src="{{asset('frontend/images/what-we-do-section.jpg')}}" alt="">
            </div>
         </section>
        <!--=================================
 about -->
<!--=================================
about-05-split  -->

<section class="about-05-split split-section gray-bg page-section-ptb mt-80">
    <div class="side-background">
      <div class="col-lg-6 img-side img-left">
           <div class="img-holder img-cover" style="background-image: url({{asset('frontend/images/prevention.jpg')}});">
          </div>
        </div>
    </div>
    <div class="container">
        <div class="row justify-content-end">
       <div class="col-lg-6 sm-mt-30 pl-70">
         <div class="shop-split-content">
           <h2 class="mt-10">Prevention </h2>
           <p>The very first and foremost effort to achieve peace is to prevent the escalation of emerging or existing conflicts to a violent level within society. GPUH aims to get involved to prevent these conflicts by identifying the problems that could lead to disagreements, disputes or violence, help in designing and implementation of policies to positively transform these conflicts, and early warning systems aimed at preventing the eruption of violence and its effects on civilians.</p>
          </div>
         </div>
      </div>
    </div>
</section>

<section class="about-05-split split-section gray-bg page-section-ptb">
  <div class="side-background">
    <div class="col-lg-6 img-side img-right">
         <div class="img-holder img-cover" style="background-image: url({{asset('frontend/images/peace-keeping.jpg')}});">
        </div>
      </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-lg-5 sm-mb-30">
         <div class="shop-split-content">
           <h2 class="mt-10">Peacekeeping </h2>
           <p>Peacekeeping is far more than just military operations to maintain peace. We consider that involvement of civilians is equally important to contain the conflict and open spaces for peacebuilding. Peacekeeping is indeed any activity that seeks to reduce violence and create a safe environment for other peacebuilding activities to take place. GPUH will engage with governments, public authorities, human right activists, peacebuilders and other influential people to achieve these peacekeeping objectives.
             </p>
         </div>
         </div>
      </div>
    </div>
</section>

<section class="about-05-split split-section gray-bg page-section-ptb">
    <div class="side-background">
      <div class="col-lg-6 img-side img-left">
           <div class="img-holder img-cover" style="background-image: url({{asset('frontend/images/peace-making.jpg')}});">
          </div>
        </div>
    </div>
    <div class="container">
        <div class="row justify-content-end">
       <div class="col-lg-6 sm-mt-30 pl-70">
         <div class="shop-split-content">
           <h2 class="mt-10">Peacemaking </h2>
           <p>Peacemaking is the process that involve efforts made to advance negotiations leading to a peace agreement that would put an end to conflict. GPUH will involve and contribute to the efforts that form part of the peace process; mediation efforts; negotiations; peace agreements; and conditions that establish the DDR (Disarmament, Demobilization and Reintegration) and the post-conflict phase.
        </p>
          </div>
         </div>
      </div>
    </div>
</section>

<section class="about-05-split split-section gray-bg page-section-ptb">
  <div class="side-background">
    <div class="col-lg-6 img-side img-right">
         <div class="img-holder img-cover" style="background-image: url({{asset('frontend/images/peace-building.jpg')}});">
        </div>
      </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-lg-5 sm-mb-30">
         <div class="shop-split-content">
           <h2 class="mt-10">Peacebuilding </h2>
           <p>Peacebuilding involves efforts being made to build a durable and sustainable peace and to prevent the resurgence of conflict. GPUH will involve, encourage and contribute to efforts including the implementation of peace agreements, the reconstruction of society, the disarmament, demobilization and reintegration (DDR), the political and social reforms that seek to address structural problems that underlie the conflict, the promotion of alternative development, the promotion of a culture of peace, the process of transitional justice and an accurate account of the past, and the promotion of the dynamics of reconciliation in a society fractured by conflict.
             </p>
         </div>
         </div>
      </div>
      <div class="col-lg-6"></div>
    </div>
</section>
<!--=================================
about-05-split  -->
<section class="page-section-pt">
    <div class="container">
       <div class="row justify-content-center text-center">
          <div class="col-lg-12 col-md-10 pb-80">
             <h3 class="mt-20" style="font-weight: 100;">GPUH will achieve its vision and mission by raising awareness about the matters that cause conflict among individuals, communities and nations. We do all what we do in the light of national & international law. We intend to raise this awareness by arranging educational seminars, virtual events, campaigns and promotions on various Digital and Print media.</h3>
             <br>
             <h3>
                Become part of this great cause and let's build a better, peaceful and united world.
             </h3>
          </div>
       </div>
    </div>
 </section>
@endsection
