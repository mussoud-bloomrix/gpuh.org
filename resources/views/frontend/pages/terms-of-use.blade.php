@extends('frontend.layouts.layout')

@section('head')
<title>Global Peace & Unity for Humanity</title>
<meta name="description" content="GPUH">
<meta name="keywords" content="GPUH">
@endsection
@section('breadcrumbs')
<ul class="page-breadcrumb">
    <li><a href="#"><i class="fa fa-home"></i> Home</a> <i class="fa fa-angle-double-right"></i></li>
    <li><a href="#">Terms Of Use</a></li>
  </ul>
  @endsection
@section('content')
@include('frontend.partials.header',
['title'=> 'Terms Of Use', 'subtitle'=>'','prefix' => 'frontend/images/', 'image'=> 'terms-of-use.jpg'
])
<div class="container my-5 mt-80 mb-80">
    <p>We welcome all users in the GPUH and let you know that the following Terms of Service will apply to you as a user of the service, where the aim is to identify the essential principles of the use of smooth that achieves the best user as the successful efforts of the expected service provided by the site. The purpose of the terms and conditions of the site is to determine your obligations as a user when you use the site.</p>
    <p></p>
    <ul class="ml-4">
        <li class="mt-15">The User Conditions apply to you without looking at how to reach your constituency website.</li>
        <li class="mt-15">All intellectual property rights, printing and dissemination of information and signs the website are protected according to the laws of the Organization of the United Kingdom.</li>
        <li class="mt-15">Do not entitled to the publication of any material on the website by any user, and the user bears the full responsibility for doing so.</li>
        <li class="mt-15">The terms of the user's electronic control circuit of the site by and construed in accordance with the laws of the United Kingdom.</li>
        <li class="mt-15">User agrees not to send by e-mail or download through the GPUH services and charity work any virus software or other intent to destroy files or data or software or hardware and computer systems or communications equipment., Or use of any content that is not authorized him using it whole or in part, or that it had no right to publish and reproduce, or includes a violation or breach of the intellectual property rights of others.</li>
        <li class="mt-15">You will not use the materials presented on the site to make copies, produce or publish or sell all or some of the material for commercial purposes. You will not use the materials in violation of any national or international laws in place, or violates any Internet-related protocols.</li>
        <li class="mt-15">the department is not responsible for any technical malfunctions or failures affecting the equipment or programs of any kind or for the loss or lack of secure communication with the network or incomplete or error or delay in transmission, and is not responsible for any damages directly or indirectly due to visit or use its services or loss of or loss of income or benefits caused by the virus may infect the user's computer equipment to access the location.</li>
        <li class="mt-15">GPUH has the right to cancel, terminate or suspend the access and use of location for any reason whatsoever and without prior notice either that or give reasons for the reservation in accordance with the principle of privacy and confidentiality.</li>
        <li class="mt-15">GPUH reserves the right to update and change these terms and conditions at any time.</li>

    </ul>
</div>
@endsection
