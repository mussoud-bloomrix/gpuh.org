@extends('frontend.layouts.layout')

@section('head')
<title>Who we are</title>
<meta name="description" content="GPUH">
<meta name="keywords" content="GPUH">
@endsection
@section('breadcrumbs')
<ul class="page-breadcrumb">
    <li><a href="#"><i class="fa fa-home"></i> Home</a> <i class="fa fa-angle-double-right"></i></li>
    <li><a href="{{route('pages.who-we-are')}}">Who We Are</a></li>
  </ul>
  @endsection
@section('content')
@include('frontend.partials.header',
['title'=> 'Who We Are?', 'subtitle'=>'', 'prefix' => 'frontend/images/', 'image'=> 'who-we-are-header.jpg', 'breadcrumbs' => ['who-we-are']
])
<!--=================================
        GPUH Mission Statement-->
<section class="page-section-ptb" id="WhoWeAre">
  <div class="container">
    <div class="row">
      <div class="col-lg-4 col-md-4 col-sm-4">
        <div class="feature-text left-icon mt-60 xs-mt-20">
          <div class="feature-icon theme-color">
            <img src="{{ asset('frontend/icons/mission.png') }}" alt="" class="" height="40px" width="40px">
          </div>
          <div class="feature-info">
            <h5 class="text-back">GPUH Mission Statement</h5>
            <p class="gpuh-intro">To make world a peaceful place to live
            </p>
          </div>
        </div>
      </div>
      <div class="col-lg-4 col-md-4 col-sm-4">
        <div class="feature-text left-icon mt-60 xs-mt-20">
          <div class="feature-icon theme-color">
            <img src="{{ asset('frontend/icons/vision.png') }}" alt="" class="" height="40px" width="40px">
          </div>
          <div class="feature-info">
            <h5 class="text-back">GPUH Vision Statement</h5>
            <p class="gpuh-intro">A peaceful <br>and united <br>human race</p>
          </div>
        </div>
      </div>
      <div class="col-lg-4 col-md-4 col-sm-4">
        <div class="feature-text left-icon mt-60 xs-mt-20">
          <div class="feature-icon theme-color">
            <img src="{{ asset('frontend/icons/ethos.png') }}" alt="" class="" height="40px" width="40px">
          </div>
          <div class="feature-info">
            <h5 class="text-back">GPUH Ethos</h5>
            <p class="gpuh-intro">To do everything in the light of international laws</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!--=================================
        End GPUH Mission Statement-->

<!--=================================
        WhoWeAre-->
<section class="page-section-ptb gray-bg">
  <div class="container">
    <div class="row">
      <div class="col-lg-6">
        <img src="{{ asset('frontend/images/who-we-are.jpg') }}" class="img-fluid full-width" alt="">
      </div>
      <div class="col-lg-6 sm-mt-30">
        <div class="section-title">
          <h2>Who We Are</h2>
        </div>
        <p class="text-custom">As our name explains, Global Peace & Unity for Humanity is about a Peaceful and a United world. We want to bring more peace and unity into this world. Whether it’s between individuals, among communities or nations, we are committed and driven to promote peace by supporting the compassionate understanding of others, respectful assertion of needs, and collaborative dialogue in conflict.</p>
      </div>
    </div>
  </div>
</section>
<!--=================================
        End WhoWeAre-->
<!--=================================
 about -->

<section class="blockquote-section page-section-ptb">
  <div class="container">
    <div class="row no-gutter">
      <div class="col-sm-7 text-left blockquote-section-left">
        <blockquote class="blockquote quote">
            Peace is a daily, a weekly, a monthly process, gradually changing opinions, slowly eroding old barriers, quietly building new structures.
          <cite>- John F. Kennedy <span class="text-gray"> 35th President of the United States</span></cite>
        </blockquote>
      </div>
      <div class="col-sm-5 blockquote-section-right">
        <img class="img-fluid" src="{{ asset('frontend/images/about/president.jpg') }}" alt="">
      </div>
    </div>
  </div>
</section>

<!--=================================
 about -->
<!--=================================
 portfolio -->

<section class="white-bg o-hidden popup-gallery">
  <div class="container-fluid p-0">
    <div class="row">
      <div class="col-sm-12 text-center">
        <div class="owl-carousel" data-items="4" data-md-items="4" data-sm-items="3" data-xs-items="2" data-xx-items="1" data-space="0">
          <div class="item">
            <div class="portfolio-item">
              <img src="{{asset('frontend/images/portfolio/small/1.jpg')}}" alt="">
              <a class="popup portfolio-img" href="{{asset('frontend/images/portfolio/small/1.jpg')}}"><i class="fa fa-arrows-alt"></i></a>
            </div>
          </div>
          <div class="item">
            <div class="portfolio-item">
              <img src="{{asset('frontend/images/portfolio/small/2.jpg')}}" alt="">
              <a class="popup portfolio-img" href="{{asset('frontend/images/portfolio/small/2.jpg')}}"><i class="fa fa-arrows-alt"></i></a>
            </div>
          </div>
          <div class="item">
            <div class="portfolio-item">
              <img src="{{asset('frontend/images/portfolio/small/3.jpg')}}" alt="">
              <a class="popup portfolio-img" href="{{asset('frontend/images/portfolio/small/3.jpg')}}"><i class="fa fa-arrows-alt"></i></a>
            </div>
          </div>
          <div class="item">
            <div class="portfolio-item">
              <img src="{{asset('frontend/images/portfolio/small/4.jpg')}}" alt="">
              <a class="popup portfolio-img" href="{{asset('frontend/images/portfolio/small/4.jpg')}}"><i class="fa fa-arrows-alt"></i></a>
            </div>
          </div>
          <div class="item">
            <div class="portfolio-item">
              <img src="{{asset('frontend/images/portfolio/small/5.jpg')}}" alt="">
              <a class="popup portfolio-img" href="{{asset('frontend/images/portfolio/small/5.jpg')}}"><i class="fa fa-arrows-alt"></i></a>
            </div>
          </div>

        </div>
      </div>
    </div>
  </div>
</section>

<!--=================================
 portfolio -->
<!--=================================
 counter -->

<section class="black-bg page-section-ptb">
  <div class="container">
    <div class="row">
      <div class="col-lg-3 col-sm-6 sm-mb-40">
        <div class="counter big-counter text-center">
          <span class="timer theme-color" data-to="3" data-speed="10">3</span>
          <label class="text-white">Initiatives</label>
        </div>
      </div>
      <div class="col-lg-3 col-sm-6 sm-mb-40">
        <div class="counter big-counter text-center">
          <span class="timer theme-color" data-to="3" data-speed="10">3</span>
          <label class="text-white">Completed Campaigns</label>
        </div>
      </div>
      <div class="col-lg-3 col-sm-6 xs-mb-40">
        <div class="counter big-counter text-center">
          <span class="timer theme-color" data-to="10" data-speed="10">10</span>
          <label class="text-white">New Campaigns</label>
        </div>
      </div>
      <div class="col-lg-3 col-sm-6">
        <div class="counter big-counter text-center">
          <span class="timer count-100 theme-color" data-to="100" data-speed="10">100</span>
          <label class="text-white">Ambassadors</label>
        </div>
      </div>
    </div>
  </div>
</section>

<!--=================================
 counter -->
<!--=================================
 feature -->



<!--=================================
 custom -->

<!--=================================
 play-video -->

<section class="page-section-pt bg-overlay-theme-90 parallax" style="background-image: url({{asset('frontend/images/our-initiatives-background.jpg')}});">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-9">
        <div class="play-video-section text-center">
          <div class="play-video text-center">
            <a class="view-video popup-youtube" href="https://www.youtube.com/watch?v=rDyy5nXdHrU"> <i class="fa fa-play"></i> </a>
          </div>
          <div class="content mt-40">
            <h2 class="text-white mb-20">We believe in making an impact, not an impression.</h2>
            <h5 class="text-white mb-50"><i>Want to know more? Watch video.</i></h5>
            <img class="img-fluid mx-auto" src="{{asset('frontend/images/objects/video-thumbnail.png')}}" alt="">
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<!--=================================
 play-video -->

@endsection

@push('scripts')
<script src="{{ asset('frontend/js/counter/jquery.countTo.js')}}"></script>
<script type="text/javascript">
    $('.count-100').countTo({
    onComplete: function (value) {
        $('.count-100').text(value+'+')
    }
    });
  </script>
@endpush
