@extends('frontend.layouts.layout')

@section('head')
<title>Global Peace & Unity for Humanity</title>
<meta name="description" content="GPUH">
<meta name="keywords" content="GPUH">
@endsection
@section('breadcrumbs')
<ul class="page-breadcrumb">
    <li><a href="#"><i class="fa fa-home"></i> Home</a> <i class="fa fa-angle-double-right"></i></li>
    <li><a href="{{route('pages.initiatives')}}">Initiatives</a></li>
  </ul>
@endsection
@section('content')
@include('frontend.partials.header',
['title'=> 'Initiatives', 'subtitle'=>'', 'prefix'=> 'frontend/images/', 'image' => 'initiative-banner.jpg'
])

@include('frontend.partials.basic-listing-card', ['records' => $initiatives])
@endsection
