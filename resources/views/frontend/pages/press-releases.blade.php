@extends('frontend.layouts.layout')

@section('head')
<title>Global Peace & Unity for Humanity</title>
<meta name="description" content="GPUH">
<meta name="keywords" content="GPUH">
@endsection
@section('breadcrumbs')
<ul class="page-breadcrumb">
    <li><a href="#"><i class="fa fa-home"></i> Home</a> <i class="fa fa-angle-double-right"></i></li>
    <li><a href="{{route('pages.press-releases')}}">Press Release</a></li>
</ul>
@endsection
@section('content')
@include('frontend.partials.header',
['title'=> 'Press Release', 'subtitle'=> '', 'prefix' => 'frontend/', 'image'=> 'images/press-release.jpg'
])

<section class="blog blog-grid-3-column white-bg page-section-ptb">
    <div class="container">
        <div class="row">
            @forelse ($pressReleases as $pressRelease)
            <div class="col-lg-4 col-md-4">
                <div class="blog-entry mb-50">
                    <div class="entry-image clearfix">
                        <a href="{{route('pages.press-release', $pressRelease->slug )}}">
                            <img class="img-fluid" src="{{ asset('storage/' . $pressRelease->featured_image) }}" alt="">
                        </a>
                    </div>
                    <div class="blog-detail">
                        <div class="entry-title mb-10">
                            <a
                                href="{{route('pages.press-release', $pressRelease->slug )}}">{{ $pressRelease->title }}</a>
                        </div>
                        <div class="entry-meta mb-10">
                            <ul>
                                <li> <i class="fa fa-folder-open-o"></i>
                                    @foreach($pressRelease->campaigns as $key => $campaign)
                                    <a
                                        href="{{route('pages.campaign', [$campaign->initiative->slug, $campaign->slug])}}">
                                        {{ $campaign->name }} @if(count($pressRelease->campaigns) != $key+1), @endif
                                    </a>
                                    @endforeach
                                </li>
                                <li><a href="#"><i class="fa fa-calendar-o"></i>
                                        {{ \Carbon\Carbon::parse($pressRelease->date)->format('M d, Y') }}</a>
                                </li>
                            </ul>
                        </div>
                        <div class="entry-content">
                            <p>{!! $pressRelease->excerpt !!}</p>
                        </div>
                        <div class="entry-share clearfix">
                            <div class="entry-button">
                                <a class="button arrow"
                                    href="{{route('pages.press-release', $pressRelease->slug )}}">Read More<i
                                        class="fa fa-angle-right" aria-hidden="true"></i></a>
                            </div>
                            <div class="social list-style-none float-right">
                                <strong>Share : </strong>
                                <ul>
                                    <li>
                                        <a href="#"> <i class="fa fa-facebook"></i> </a>
                                    </li>
                                    <li>
                                        <a href="#"> <i class="fa fa-twitter"></i> </a>
                                    </li>
                                    <li>
                                        <a href="#"> <i class="fa fa-instagram"></i> </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        @empty
                        @endforelse
                    </div>
                </div>
</section>
<!-- ================================================ -->

@endsection
