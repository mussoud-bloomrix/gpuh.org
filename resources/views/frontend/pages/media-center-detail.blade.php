@extends('frontend.layouts.layout')

@section('head')
<title>{{$media->title}}</title>
<meta name="description" content="{{$media->description}}">
<meta name="keywords" content="{{ $media->keywords }}">
@endsection
@section('breadcrumbs')
<ul class="page-breadcrumb">
    <li><a href="#"><i class="fa fa-home"></i> Home</a> <i class="fa fa-angle-double-right"></i></li>
    @if(request()->routeIs('pages.media-center-detail'))
    <li><a href="#">Initiative</a><i class="fa fa-angle-double-right"></i></li>
    <li><a href="{{route('pages.initiative', $initiative->slug)}}">{{$initiative->name}}</a><i
            class="fa fa-angle-double-right"></i></li>
    <li><a href="{{route('pages.initiative', $initiative->slug, $campaign->slug)}}">{{$campaign->name}}</a><i
            class="fa fa-angle-double-right"></i></li>
    <li><a href="{{route('pages.initiative', $initiative->slug, $campaign->slug, $media->slug)}}">{{$media->title}}</a>
    </li>
    @else
    <li><a href="{{route('pages.' . $breadcrumb)}}">{{str_replace('-', ' ', $breadcrumb)}}</a><i
            class="fa fa-angle-double-right"></i></li>
    <li><a href="#">{{$media->title}}</a></li>
    @endif
</ul>
@endsection
@section('content')
@include('frontend.partials.header',
['title'=> $media->title, 'subtitle'=> '', 'prefix' => 'storage/', 'image'=> $media->header_image
])

<section class="blog blog-single white-bg page-section-ptb">
    <div class="container">
        <div class="row">
            <div class="col-lg-9">
                <div class="blog-entry mb-10">
                    @if($media->video_url)
                    <div class="blog-entry-you-tube">
                        <div class="js-video [youtube, widescreen]">
                            <iframe src="{{$media->video_url}}" allowfullscreen></iframe>
                        </div>
                    </div>
                    @else
                    <div class="entry-image clearfix">
                        <img class="img-fluid" src="{{asset('storage/' . $media->image)}}" alt="">
                    </div>
                    @endif
                    <div class="blog-detail">
                        <div class="entry-meta mb-10">
                            <ul>
                                <li> <i class="fa fa-folder-open-o"></i>
                                    @foreach($media->campaigns as $key => $campaign)
                                    <a
                                        href="{{route('pages.campaign', [$campaign->initiative->slug, $campaign->slug])}}">
                                        {{ $campaign->name }} @if(count($media->campaigns) != $key+1), @endif
                                    </a>
                                    @endforeach
                                </li>
                                <li><a href="#"><i
                                            class="fa fa-calendar-o"></i>{{ \Carbon\Carbon::parse($media->date)->format('M d, Y') }}</a>
                                </li>
                            </ul>
                        </div>
                        <div class="entry-content">
                            {!! $media->introduction !!}
                        </div>
                    </div>
                </div>
                <!-- ================================================ -->
                <div class="entry-image clearfix my-5">
                    <div class="owl-carousel bottom-center-dots" data-nav-dots="ture" data-items="1" data-md-items="1"
                        data-sm-items="1" data-xs-items="1" data-xx-items="1">
                        @if($media->sliderImages())
                        @foreach ($media->sliderImages() as $sliderImage)
                        <div class="item">
                            <img class="img-fluid" src="{{ asset('storage/' . $sliderImage)}}" alt="">
                        </div>
                        @endforeach
                        @endif
                    </div>
                    <div class="entry-content my-5">
                        {!! $media->body !!}
                    </div>
                    @if($relatedPosts)
                    <div class="related-work mt-40 my-5">
                        <div class="row">
                            <div class="col-ld-12 col-md-12">
                                <h3 class="theme-color mb-20">Related Post</h3>
                                <div class="owl-carousel" data-nav-dots="false" data-items="2" data-xs-items="1"
                                    data-xx-items="1">
                                    @forelse ($relatedPosts as $relatedPost)
                                    <div class="item">
                                        <div class="blog-box blog-1 active">
                                            <div class="blog-info">
                                                <span class="post-category">
                                                    @foreach($media->campaigns as $key => $campaign)
                                                    <a
                                                        href="{{route('pages.campaign', [$campaign->initiative->slug, $campaign->slug])}}">
                                                        {{ $campaign->name }}
                                                    </a>
                                                    @endforeach
                                                </span>
                                                <h4> <a
                                                        href="{{route('pages.media-center-detail',[$initiative->slug, $campaign->slug, $relatedPost->postable->slug])}}">{{$relatedPost->postable->title}}</a>
                                                </h4>
                                                <p>{{$relatedPost->postable->tagline}}
                                                </p>
                                                <span><i class="fa fa-user"></i> <a class="text-white"
                                                        href="{{route('pages.campaign', [$initiative->slug, $campaign->slug])}}">{{$campaign->name}}</a></span>
                                                <span><i class="fa fa-calendar-check-o"></i>
                                                    {{ \Carbon\Carbon::parse($relatedPost->postable->created_at)->format('M d, Y') }}
                                                </span>
                                            </div>
                                            <div class="entry-content">
                                                {!! $media->introduction !!}
                                            </div>
                                        </div>
                                    </div>
                                    <!-- ================================================ -->
                                    <div class="entry-image clearfix my-5">
                                        <div class="owl-carousel bottom-center-dots" data-nav-dots="ture" data-items="1"
                                            data-md-items="1" data-sm-items="1" data-xs-items="1" data-xx-items="1">
                                            @if($media->sliderImages())
                                            @foreach ($media->sliderImages() as $sliderImage)
                                            <div class="item">
                                                <img class="img-fluid" src="{{ asset('storage/' . $sliderImage)}}"
                                                    alt="">
                                            </div>
                                            @endforeach
                                            @endif
                                        </div>
                                        <div class="entry-content my-5">
                                            {!! $media->body !!}
                                        </div>
                                        @if($relatedPosts)
                                        <div class="related-work mt-40 my-5">
                                            <div class="row">
                                                <div class="col-ld-12 col-md-12">
                                                    <h3 class="theme-color mb-20">Related Post</h3>
                                                    <div class="owl-carousel" data-nav-dots="false" data-items="2"
                                                        data-xs-items="1" data-xx-items="1">
                                                        @forelse ($relatedPosts as $relatedPost)
                                                        <div class="item">
                                                            <div class="blog-box blog-1 active">
                                                                <div class="blog-info">
                                                                    <span class="post-category">
                                                                        @foreach($media->campaigns as $key => $campaign)
                                                                        <a
                                                                            href="{{route('pages.campaign', [$campaign->initiative->slug, $campaign->slug])}}">
                                                                            {{ $campaign->name }}
                                                                        </a>
                                                                        @endforeach
                                                                    </span>
                                                                    <h4> <a
                                                                            href="{{route('pages.media-center-detail',[$initiative->slug, $campaign->slug, $relatedPost->postable->slug])}}">{{$relatedPost->postable->title}}</a>
                                                                    </h4>
                                                                    <p>{{$relatedPost->postable->tagline}}
                                                                    </p>
                                                                    <span><i class="fa fa-user"></i> <a
                                                                            class="text-white"
                                                                            href="{{route('pages.campaign', [$initiative->slug, $campaign->slug])}}">{{$campaign->name}}</a></span>
                                                                    <span><i class="fa fa-calendar-check-o"></i>
                                                                        {{ \Carbon\Carbon::parse($relatedPost->postable->created_at)->format('M d, Y') }}
                                                                    </span>
                                                                </div>
                                                                <div class="blog-box-img"
                                                                    style="background-image:url({{asset('storage/' . $relatedPost->postable->featured_image)}});">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        @empty
                                                        @endforelse
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- ================================================ -->
                                        @endif
                                    </div>
                                </div>
                                <!-- ================================================ -->
                                <div class="col-lg-3">
                                    <div class="sidebar-widget">
                                        <h6 class="mt-40 mb-20">Recent Posts </h6>
                                        @forelse ($recentPosts as $recentPost)
                                        <div class="recent-post clearfix">
                                            <div class="recent-post-image">
                                                <img class="img-fluid"
                                                    src="{{asset('storage/' . $recentPost->featured_image)}}" alt="">
                                            </div>
                                            <div class="recent-post-info">
                                                @if ($breadcrumb = 'latest-news')
                                                <a
                                                    href="{{route('pages.latest-news-detail', $recentPost->slug )}}">{{ $recentPost->title }}</a>
                                                @elseif ( $breadcrumb = 'in-the-news')
                                                <a
                                                    href="{{route('pages.in-the-news-detail', $recentPost->slug )}}">{{ $recentPost->title }}</a>
                                                @else
                                                <a
                                                    href="{{route('pages.media-center-detail',[$initiative->slug, $campaign->slug, $recentPost->slug])}}">{{ $recentPost->title }}</a>
                                                @endif
                                                <span><i class="fa fa-calendar-o"></i>
                                                    {{ \Carbon\Carbon::parse($recentPost->created_at)->format('M d, Y') }}</span>
                                            </div>
                                        </div>
                                        @empty
                                        @endforelse

                                    </div>
                                </div>
                                <!-- ========================== -->
                            </div>
                        </div>
                    </div>
</section>
@endsection
