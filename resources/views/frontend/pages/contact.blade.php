@extends('frontend.layouts.layout')

@section('head')
<title>Contact | Global Peace & Unity for Humanity</title>
<meta name="description" content="GPUH">
<meta name="keywords" content="GPUH">
@endsection

@section('breadcrumbs')
<ul class="page-breadcrumb">
    <li><a href="#"><i class="fa fa-home"></i> Home</a> <i class="fa fa-angle-double-right"></i></li>
    <li><a href="{{route('pages.contact')}}">Contact</a></li>
  </ul>
@endsection

@section('content')
@include('frontend.partials.header',
['title'=> 'Contact', 'subtitle'=>'', 'prefix' => 'frontend/images/', 'image'=> 'contact-banner.jpg'
])
<!--=================================
 contact-->

 <section class="theme-bg contact-2 clearfix o-hidden">
    <div class="container-fluid pos-r">
     <div class="row">
     <div class="col-lg-6 map-side map-right">
         <img src="{{ asset('frontend/images/contact-side.jpg') }}">
     </div>
    </div>
   </div>
   <div class="container">
   <div class="row">
       <div class="col-lg-5">
       <div class="contact-3-info page-section-ptb text-white">
        <div class="clearfix">
            <h2 class="text-white">Contact Info</h2>
            <p class="mb-50 text-white">It would be great to hear from you! If you got any questions, please do not hesitate to send us a message.</p>

            <ul class="addresss-info list-unstyled">
             <li><i class="ti-map-alt"></i> <p>Kemp House - 152 - 160 City Road - London - EC1V 2NX</p> </li>
             <li><i class="ti-mobile"></i>+44 (0)207 11 289 11</li>
             <li><i class="ti-email"></i> General Enquiries - info@gpuh.org </li>
             <li class="ml-30"> Admin Enquiries - admin@gpuh.org </li>
             <li class="ml-30">Media Enquiries - media@gpuh.org</li>
             <li class="ml-30">Legal Enquiries - legal@gpuh.org</li>
           </ul>
        </div>
          </div>
       </div>
      </div>
     </div>
 </section>

 <section class="page-section-ptb contact-2" id="contact-form">
   <div class="container">
   <div class="row justify-content-center text-center mb-50">
   <div class="col-md-8">
       <div class="section-title mb-0">
        <h2 class="title-effect">Fill in the form below</h2>
        @include('common.partials.flash')
        </div>
    </div>
    </div>
   <div class="row">
   <div class="col-lg-12">
      <form role="form" method="post" action="{{route('pages.contact-mail')}}">
        @csrf
       <div class="contact-form clearfix">
         <div class="section-field">
           <input id="name" type="text" placeholder="Name*" class="form-control"  name="name">
          </div>
          <div class="section-field">
             <input type="email" placeholder="Email*" class="form-control" name="email">
           </div>
          <div class="section-field">
             <input type="text" placeholder="Phone*" class="form-control" name="phone">
           </div>
          <div class="section-field textarea">
            <textarea class="input-message form-control" placeholder="Message*"  rows="7" name="message"></textarea>
           </div>
             <!-- Google reCaptch-->
             <!-- <div class="g-recaptcha section-field clearfix" data-sitekey="[Add your site key]"></div> -->
             <div class="section-field submit-button">
                <button type="submit" class="button"><span> Send message </span> <i class="fa fa-paper-plane"></i></button>
            </div>
           </div>
         </form>
       </div>
      </div>
     </div>
 </section>

 <!--=================================
  contact-->
@endsection
