@extends('frontend.layouts.layout')

@section('head')
<title>Global Peace & Unity for Humanity</title>
<meta name="description" content="GPUH">
<meta name="keywords" content="GPUH">
@endsection
@section('breadcrumbs')
<ul class="page-breadcrumb">
    <li><a href="#"><i class="fa fa-home"></i> Home</a> <i class="fa fa-angle-double-right"></i></li>
    <li><a href="{{route('pages.initiatives')}}">Initiatives</a><i class="fa fa-angle-double-right"></i></li>
    <li><a href="{{route('pages.initiatives', $initiative->slug)}}">{{$initiative->name}}</a></li>
    <li><a href="{{route('pages.campaign', [$initiative->slug, $campaign->slug])}}">{{$campaign->name}}</a></li>
</ul>
@endsection
@section('content')
@include('frontend.partials.header',
['title'=> $campaign->name, 'subtitle'=>'', 'prefix' => 'storage/', 'image'=> $campaign->header_image
])

{!! $campaign->body !!}
@if(count($mediaList) > 0)
<section class="page-section-pt">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 mt-30">
                <div class="section-title line center text-center">
                    <h2 class="title">Media Posts</h2>
                </div>
            </div>
        </div>
    </div>
</section>
@endif
<section class="blog blog-grid-3-column white-bg page-section-ptb">
    <div class="container">
        <div class="row">
            @php
            @endphp
            @forelse ($mediaList as $media)
            <div class="col-lg-4 col-md-4">
                <div class="blog-entry mb-50">
                    <div class="entry-image clearfix">
                        <a
                            href="{{route('pages.media-center-detail',[$initiative->slug, $campaign->slug, $media->postable->slug])}}">
                            <img class="img-fluid" src="{{ asset('storage/' . $media->postable->featured_image) }}"
                                alt="">
                        </a>
                    </div>
                    <div class="blog-detail">
                        <div class="entry-title mb-10">
                            <a
                                href="{{route('pages.media-center-detail',[$initiative->slug, $campaign->slug, $media->postable->slug])}}">{{ $media->postable->title }}</a>
                        </div>
                        <div class="entry-meta mb-10">
                            <ul>
                                <li> <i class="fa fa-folder-open-o"></i> <a href="#"> {{ $initiative->name }}</a> </li>
                                <li><a href="#"><i class="fa fa-calendar-o"></i>
                                        {{ \Carbon\Carbon::parse($media->postable->date)->format('d-M-Y') }}</a></li>
                            </ul>
                        </div>
                    </div>
                    @empty
                    @endforelse
                </div>
            </div>
</section>



@endsection
