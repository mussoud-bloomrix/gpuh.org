@extends('frontend.layouts.layout')

@section('head')
<title>Global Peace & Unity for Humanity</title>
<meta name="description" content="GPUH">
<meta name="keywords" content="GPUH">
@endsection
@section('breadcrumbs')
<ul class="page-breadcrumb">
    <li><a href="#"><i class="fa fa-home"></i> Home</a> <i class="fa fa-angle-double-right"></i></li>
    <li><a href="{{route('pages.latest-news')}}">Latest News</a></li>
</ul>
@endsection
@section('content')
@include('frontend.partials.header',
['title'=> 'Latest news', 'subtitle'=> '', 'prefix' => 'frontend/', 'image'=> 'images/latest-news.jpg'])

<section class="blog blog-grid-3-column white-bg page-section-ptb">
    <div class="container">
        <div class="row">
            @forelse ($latestNews as $media)
            <div class="col-lg-4 col-md-4">
                <div class="blog-entry mb-50">
                    <div class="entry-image clearfix">
                        <a href="{{route('pages.latest-news-detail', $media->slug )}}">
                            <img class="img-fluid" src="{{ asset('storage/' . $media->featured_image) }}" alt="">
                        </a>
                    </div>
                    <div class="blog-detail">
                        <div class="entry-title mb-10">
                            <a href="{{route('pages.latest-news-detail', $media->slug )}}">{{ $media->title }}</a>
                        </div>
                        <div class="entry-meta mb-10">
                            <ul>
                                <li> <i class="fa fa-folder-open-o"></i>
                                    @foreach($media->campaigns as $key => $campaign)
                                    <a
                                        href="{{route('pages.campaign', [$campaign->initiative->slug, $campaign->slug])}}">
                                        {{ $campaign->name }} @if(count($media->campaigns) != $key+1), @endif
                                    </a>
                                    @endforeach
                                </li>
                                <li><a href="#"><i class="fa fa-calendar-o"></i>
                                        {{ \Carbon\Carbon::parse($media->date)->format('M d, Y') }}</a></li>
                            </ul>
                        </div>
                        <div class="entry-content">
                            <p>{!! $media->excerpt !!}</p>
                        </div>
                        <div class="entry-share clearfix">
                            <div class="entry-button">
                                <a class="button arrow" href="{{route('pages.latest-news-detail', $media->slug )}}">Read
                                    More<i class="fa fa-angle-right" aria-hidden="true"></i></a>
                            </div>
                            <div class="social list-style-none float-right">
                                <strong>Share : </strong>
                                <ul>
                                    <li>
                                        <a href="#"> <i class="fa fa-facebook"></i> </a>
                                    </li>
                                    <li>
                                        <a href="#"> <i class="fa fa-twitter"></i> </a>
                                    </li>
                                    <li>
                                        <a href="#"> <i class="fa fa-instagram"></i> </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        @empty
                        @endforelse
                    </div>
                </div>
</section>
<!-- ================================================ -->

@endsection
