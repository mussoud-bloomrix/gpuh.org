<section class="page-title bg-overlay-black-60 parallax" data-jarallax='{"speed": 0.6}' style="background-image:url('{{ asset(($prefix ?? ''). ($image ?? '')) }}');">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <div class="page-title-name">
          <h1 class="header-title">{{$title ?? ''}}</h1>
          <p>{{$subtitle ?? ''}}</p>
        </div>
        @yield('breadcrumbs')
      </div>
    </div>
  </div>
</section>
