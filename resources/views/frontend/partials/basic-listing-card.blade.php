<section class="blog blog-grid-3-column white-bg page-section-ptb">
    <div class="container">
      <div class="row">
          @forelse ($records as $record)
          <div class="col-lg-4 col-md-4">
            <div class="blog-entry mb-50">
              <div class="entry-image clearfix">
                <a href="{{route('pages.initiative', $record->slug)}}">
                    <img class="img-fluid" src="{{ asset('storage/' . $record->feature_image) }}" alt="">
                </a>
              </div>
              <div class="blog-detail">
                <div class="entry-title mb-10">
                  <a href="{{route('pages.initiative', $record->slug)}}">{{ $record->name }}</a>
                </div>
                <div class="entry-content">
                  <div>{{$record->excerpt}} </div>
                </div>
                <div class="entry-share clearfix">
                  <div class="entry-button">
                    <a class="button arrow" href="{{route('pages.initiative', $record->slug)}}">Read More<i class="fa fa-angle-right" aria-hidden="true"></i></a>
                  </div>
                </div>
              </div>
            </div>

          </div>
          @empty
          @endforelse
      </div>
    </div>
  </section>
  <!-- ================================================ -->
