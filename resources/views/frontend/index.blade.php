@extends('frontend.layouts.layout')

@section('styles')
    <style>
        .text-custom {
            color: #818080;
        }
        h1.text-custom {
            font-size: 50px;
        }
        ::selection {
            background: #2580c4;
            color: #fff;
            text-shadow: none;
        }
    </style>
@endsection

@section('content')
<div class="wrapper">

    <!--=================================
        preloader -->

    <div id="pre-loader">
        <img src="{{asset('frontend/images/pre-loader/2.svg')}}" alt="">
    </div>

    <!--=================================
        preloader -->

    <div class="st-pusher">
        <!--=================================
            header -->

        <header id="header" class="header default fullWidth" style="background-color: #ffffff; position: relative;">
            <div class="topbar">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12 col-md-12">
                            <div class="topbar-social text-center text-md-center">
                                <img src="{{asset('frontend/images/logo.png')}}" style="width: 325px;" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>

        <!--=================================
            header -->

        <!--=================================
            banner -->

        <section class="rev-slider">
            <div id="rev_slider_268_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-alias="webster-slider-2" data-source="gallery" style="margin:0px auto;background:transparent;padding:0px;margin-top:0px;margin-bottom:0px;">
                <!-- START REVOLUTION SLIDER 5.4.6.3 fullwidth mode -->
                <div id="rev_slider_268_1" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.4.6.3">
                    <ul>  <!-- SLIDE  -->
                        <li data-index="rs-757" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="300"  data-thumb="{{asset('frontend/images/peace.jpg')}}"  data-delay="9000"  data-rotate="0"  data-saveperformance="off"  data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                            <!-- MAIN IMAGE -->
                            <img src="{{asset('frontend/images/slider-1.jpg')}}"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>

                            <!-- LAYERS -->
                            <div class="tp-caption   tp-resizeme  rev-color"
                                id="slide-758-layer-3"
                                data-x="center" data-hoffset="-1"
                                data-y="center" data-voffset="-50"
                                data-width="['auto']"
                                data-height="['auto']"

                                data-type="text"
                                data-responsive_offset="on"

                                data-frames='[{"delay":1200,"speed":2000,"frame":"0","from":"z:0;rX:0;rY:0;rZ:0;sX:0.8;sY:0.8;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power4.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                                data-textAlign="['inherit','inherit','inherit','inherit']"
                                data-paddingtop="[0,0,0,0]"
                                data-paddingright="[0,0,0,0]"
                                data-paddingbottom="[0,0,0,0]"
                                data-paddingleft="[0,0,0,0]"

                                style="z-index: 6; white-space: nowrap; font-size: 160px; line-height: 160px; font-weight: 700; color: #ffffff; letter-spacing: 10px;font-family:Poppins;text-transform:uppercase;">
                                Welcome
                            </div>

                            <div class="tp-caption   tp-resizeme  rev-color"
                                id="slide-758-layer-3"
                                data-x="center" data-hoffset="-1"
                                data-y="center" data-voffset="150"
                                data-width="['auto']"
                                data-height="['auto']"

                                data-type="text"
                                data-responsive_offset="on"

                                data-frames='[{"delay":1200,"speed":2000,"frame":"0","from":"z:0;rX:0;rY:0;rZ:0;sX:0.8;sY:0.8;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power4.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                                data-textAlign="['center']"
                                data-paddingtop="[0,0,0,0]"
                                data-paddingright="[0,0,0,0]"
                                data-paddingbottom="[0,0,0,0]"
                                data-paddingleft="[0,0,0,0]"

                                style="z-index: 6; white-space: nowrap; font-size: 24px; line-height: 34px; color: #ffffff; font-family:Poppins;">
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Expedita, <br>
                                quia officiis excepturi, omnis aliquid soluta molestiae vitae, <br>
                                eos similique necessitatibus facilis iste deserunt! Voluptas <br>
                                dignissimos nesciunt eaque minus! dignissimos nesciunt <br>
                                dignissimos nesciunt eaque minus! dignissimos <br>
                            </div>

                        {{-- <iframe width="100%" style="height: 100vh" src="https://www.youtube.com/embed/k_QeVEeyHAM" frameborder="0" allow="autoplay;" allowfullscreen></iframe> --}}
                        </li>

                        <!-- SLIDE  -->
                        <li data-index="rs-758" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="300"  data-thumb="{{asset('frontend/images/peace.jpg')}}"  data-delay="9000"  data-rotate="0"  data-saveperformance="off"  data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                            <!-- MAIN IMAGE -->
                            <img src="{{asset('frontend/images/peace.jpg')}}"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
                            <!-- LAYERS -->
                            <!-- LAYER NR. 5 -->
                            <div class="tp-caption   tp-resizeme  rev-color"
                                id="slide-758-layer-3"
                                data-x="center" data-hoffset="-1"
                                data-y="center" data-voffset="-50"
                                data-width="['auto']"
                                data-height="['auto']"

                                data-type="text"
                                data-responsive_offset="on"

                                data-frames='[{"delay":1200,"speed":2000,"frame":"0","from":"z:0;rX:0;rY:0;rZ:0;sX:0.8;sY:0.8;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power4.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                                data-textAlign="['inherit','inherit','inherit','inherit']"
                                data-paddingtop="[0,0,0,0]"
                                data-paddingright="[0,0,0,0]"
                                data-paddingbottom="[0,0,0,0]"
                                data-paddingleft="[0,0,0,0]"

                                style="z-index: 6; white-space: nowrap; font-size: 160px; line-height: 160px; font-weight: 700; color: #ffffff; letter-spacing: 10px;font-family:Poppins;text-transform:uppercase;">
                                Peace
                            </div>
                            <div class="tp-caption   tp-resizeme  rev-color"
                                id="slide-758-layer-3"
                                data-x="center" data-hoffset="-1"
                                data-y="center" data-voffset="150"
                                data-width="['auto']"
                                data-height="['auto']"

                                data-type="text"
                                data-responsive_offset="on"

                                data-frames='[{"delay":1200,"speed":2000,"frame":"0","from":"z:0;rX:0;rY:0;rZ:0;sX:0.8;sY:0.8;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power4.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                                data-textAlign="['center']"
                                data-paddingtop="[0,0,0,0]"
                                data-paddingright="[0,0,0,0]"
                                data-paddingbottom="[0,0,0,0]"
                                data-paddingleft="[0,0,0,0]"

                                style="z-index: 6; white-space: nowrap; font-size: 24px; line-height: 34px; color: #ffffff; font-family:Poppins;">
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Expedita, <br>
                                quia officiis excepturi, omnis aliquid soluta molestiae vitae, <br>
                                eos similique necessitatibus facilis iste deserunt! Voluptas <br>
                                dignissimos nesciunt eaque minus! dignissimos nesciunt <br>
                                dignissimos nesciunt eaque minus! dignissimos <br>
                            </div>

                        </li>

                        <!-- SLIDE  -->
                        <li data-index="rs-759" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="300"  data-thumb="{{asset('frontend/images/unity.jpg')}}"  data-delay="9000"  data-rotate="0"  data-saveperformance="off"  data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                            <!-- MAIN IMAGE -->
                            <img src="{{asset('frontend/images/unity.jpg')}}"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
                            <!-- LAYERS -->

                            <!-- LAYER NR. 5 -->
                            <div class="tp-caption   tp-resizeme  rev-color"
                                id="slide-759-layer-3"
                                data-x="center" data-hoffset="-1"
                                data-y="center" data-voffset="-50"
                                data-width="['auto']"
                                data-height="['auto']"

                                data-type="text"
                                data-responsive_offset="on"

                                data-frames='[{"delay":1200,"speed":2000,"frame":"0","from":"z:0;rX:0;rY:0;rZ:0;sX:0.8;sY:0.8;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power4.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                                data-textAlign="['inherit','inherit','inherit','inherit']"
                                data-paddingtop="[0,0,0,0]"
                                data-paddingright="[0,0,0,0]"
                                data-paddingbottom="[0,0,0,0]"
                                data-paddingleft="[0,0,0,0]"

                                style="z-index: 6; white-space: nowrap; font-size: 160px; line-height: 160px; font-weight: 700; color: #ffffff; letter-spacing: 10px;font-family:Poppins;text-transform:uppercase;">
                                Unity
                            </div>
                            <div class="tp-caption   tp-resizeme  rev-color"
                                id="slide-758-layer-3"
                                data-x="center" data-hoffset="-1"
                                data-y="center" data-voffset="150"
                                data-width="['auto']"
                                data-height="['auto']"

                                data-type="text"
                                data-responsive_offset="on"

                                data-frames='[{"delay":1200,"speed":2000,"frame":"0","from":"z:0;rX:0;rY:0;rZ:0;sX:0.8;sY:0.8;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power4.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                                data-textAlign="['center']"
                                data-paddingtop="[0,0,0,0]"
                                data-paddingright="[0,0,0,0]"
                                data-paddingbottom="[0,0,0,0]"
                                data-paddingleft="[0,0,0,0]"

                                style="z-index: 6; white-space: nowrap; font-size: 24px; line-height: 34px; color: #ffffff; font-family:Poppins;">
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Expedita, <br>
                                quia officiis excepturi, omnis aliquid soluta molestiae vitae, <br>
                                eos similique necessitatibus facilis iste deserunt! Voluptas <br>
                                dignissimos nesciunt eaque minus! dignissimos nesciunt <br>
                                dignissimos nesciunt eaque minus! dignissimos <br>
                            </div>


                        </li>

                    </ul>
                    <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>
                </div>
            </div>
        </section>

        <!--=================================
            banner -->

        <!--=================================
            custom content -->

        {{-- <section class="bitcoin-custom-content parallax" style="background: url('{{ asset('frontend/images/3.png') }}');"> --}}
        <section class="bitcoin-custom-content parallax">
            <div class="container-fluid p-0">
                <div class="row row-eq-height no-gutter">
                    <div class="col-lg-6" style="background: white;">
                        <div class="bitcoin-custom-conten-box">
                            <div class="section-title">
                                <h3 class="text-custom">What is</h3>
                                <h1 class="text-custom">Freedom of</h1>
                                <h1 class="text-custom">Expression?</h1>
                            </div>
                            <br>
                            <h3 class="text-custom">ARTICLE 10</h3>
                            <p class="text-custom">
                                EUROPEAN CONVENTION ON HUMAN RIGHTS
                            </p>
                            <br>
                            <p class="text-custom">
                                1. Everyone has the right to freedom of expression. This right shall include <span class="text-danger">freedom to hold opinions</span> and <span class="text-danger">to receive and impart information and ideas</span> without interference by public authority and regardless of frontiers. This Article shall not prevent States from requiring the licensing of broadcasting, television or cinema enterprises.
                            </p>
                            <br>
                            <p class="text-custom">
                                2. The exercise of these freedoms, since it <span class="text-danger">carries with it duties and responsibilities</span>, may be subject to such formalities, conditions, restrictions or penalties as are prescribed by law and are necessary in a democratic society, in the interests of <span class="text-danger">national security, territorial integrity or public safety, for the prevention of disorder or crime, for the protection of health or morals</span>, for the protection of the reputation or rights of others, for preventing the disclosure of information received in confidence, or for maintaining the authority and impartiality of the judiciary.
                            </p>
                        </div>
                    </div>
                    <a href="https://www.echr.coe.int/documents/convention_eng.pdf" class="col-lg-6" style="">
                        <img src="{{ asset('frontend/images/banner.jpg') }}" class="img-fluid" height="100%" alt="">
                    </a>
                </div>
            </div>
        </section>

        <section class="page-section-ptb">
            <div class="container">
              <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="embed-responsive embed-responsive-21by9">
                        <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/k_QeVEeyHAM" allowfullscreen></iframe>
                    </div>
                </div>
              </div>
            </div>
        </section>

        <!--=================================
            custom content -->


        <footer class="footer footer-topbar black-bg" >
            <div class="copyright" style="padding: 10px 0 10px;">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-12 col-md-12">
                            <div class="footer-text text-center">
                                <p> &copy;Copyright <span id="copyright"> <script>document.getElementById('copyright').appendChild(document.createTextNode(new Date().getFullYear()))</script></span> Global Peace & Unity for Humanity. All Rights Reserved.  </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>

        <!--=================================
            footer -->

    </div>
</div>
@endsection
