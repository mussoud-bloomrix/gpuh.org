<!DOCTYPE html>
<html lang="en">

<head>
  @yield('head')
  <meta charset=utf-8>
  <!--CSS -->
  <link href="{{ asset('frontend/css/default.css') }}" rel="stylesheet" type="text/css">
  <!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=edge"><![endif]-->
  <meta name="robots" content="index, follow">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <meta name="theme-color" content="#2a2b2f">
  <script id="Cookiebot" src="https://consent.cookiebot.com/uc.js" data-cbid="014fab7a-3a2e-4a67-9754-6089c42d281c" data-blockingmode="auto" type="text/javascript"></script>
  <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-2570RBNLY3"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());
  gtag('config', 'G-2570RBNLY3');
</script>
  <!-- FAVICONS -->
  <link rel="apple-touch-icon" sizes="57x57" href="{{asset('frontend/images/favicon/apple-icon-57x57.png')}}">
  <link rel="apple-touch-icon" sizes="60x60" href="{{asset('frontend/images/favicon/apple-icon-60x60.png')}}">
  <link rel="apple-touch-icon" sizes="72x72" href="{{asset('frontend/images/favicon/apple-icon-72x72.png')}}">
  <link rel="apple-touch-icon" sizes="76x76" href="{{asset('frontend/images/favicon/apple-icon-76x76.png')}}">
  <link rel="apple-touch-icon" sizes="114x114" href="{{asset('frontend/images/favicon/apple-icon-114x114.png')}}">
  <link rel="apple-touch-icon" sizes="120x120" href="{{asset('frontend/images/favicon/apple-icon-120x120.png')}}">
  <link rel="apple-touch-icon" sizes="144x144" href="{{asset('frontend/images/favicon/apple-icon-144x144.png')}}">
  <link rel="apple-touch-icon" sizes="152x152" href="{{asset('frontend/images/favicon/apple-icon-152x152.png')}}">
  <link rel="apple-touch-icon" sizes="180x180" href="{{asset('frontend/images/favicon/apple-icon-180x180.png')}}">
  <link rel="icon" type="image/png" sizes="192x192" href="{{asset('frontend/images/favicon/android-icon-192x192.png')}}">
  <link rel="icon" type="image/png" sizes="32x32" href="{{asset('frontend/images/favicon/favicon-32x32.png')}}">
  <link rel="icon" type="image/png" sizes="96x96" href="{{asset('frontend/images/favicon/favicon-96x96.png')}}">
  <link rel="icon" type="image/png" sizes="16x16" href="{{asset('frontend/images/favicon/favicon-16x16.png')}}">
  <link rel="manifest" href="{{asset('frontend/images/favicon/manifest.json')}}">
  <meta name="msapplication-TileColor" content="#ffffff">
  <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
  <meta name="theme-color" content="#ffffff">
  <!-- Favicon -->
  <!-- font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,500,500i,600,700,800,900%7CPoppins:200,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Dosis:300,400,500,600,700,800">
  <!-- Plugins -->
  <link rel="stylesheet" type="text/css" href="{{asset('frontend/css/plugins-css.css')}}">
  <!-- revoluation -->
  <link rel="stylesheet" type="text/css" href="{{asset('frontend/revolution/css/settings.css')}}" media="screen">
  <!-- Typography -->
  <link rel="stylesheet" type="text/css" href="{{asset('frontend/css/typography.css')}}" />
  <!-- Shortcodes -->
  <link rel="stylesheet" type="text/css" href="{{asset('frontend/css/shortcodes/shortcodes.css')}}">
  <!-- Style -->

  <link rel="stylesheet" type="text/css" href="{{asset('frontend/demo-categories/bitcoin/css/bitcoin.css')}}" />
  <link rel="stylesheet" type="text/css" href="{{asset('frontend/css/style.css')}}" />
  <link href="{{ ('frontend/css/shop.css') }}" rel="stylesheet" type="text/css" />
  <!-- Responsive -->
  <link rel="stylesheet" type="text/css" href="{{asset('frontend/css/responsive.css')}}" />
  <!-- medical -->
  <link rel="stylesheet" type="text/css" href="{{asset('demo-categories/medical/css/medical.css')}}" />
  <link href="{{ asset('frontend/css/custom.css') }}" rel="stylesheet" type="text/css" />
  <title>Global Peace & Unity for Humanity</title>
  @yield('styles')
  <style>
    .font-size-30{
        font-size: 30px;
    }

    .letter-spacing-3{
        letter-spacing: 3px;
    }

    .footer.black-bg{
        background: #323232;
    }
  </style>
</head>

<body>
  <div class="wrapper">
    <!-- wrapper start -->
    <!--=================================
        preloader -->
    <div id="pre-loader">
      <img src="{{asset('frontend/images/pre-loader/2.svg')}}" alt="">
    </div>
    <!--=================================
        preloader -->
    <!--=================================
        header -->
    <header id="header" class="header default">
      <div class="topbar">
        <div class="container">
          <div class="row">
            <div class="col-lg-6 col-md-6 xs-mb-10">
            </div>
            <div class="col-lg-6 col-md-6">
              <div class="topbar-social text-center text-md-right">
                <ul>
                  <li><a href="https://www.facebook.com/gpuhofficial" target="_blank"><span class="ti-facebook"></span></a></li>
                  <li><a href="https://www.instagram.com/gpuhofficial/" target="_blank"><span class="ti-instagram"></span></a></li>
                  <li><a href="https://twitter.com/gpuhofficial" target="_blank"><span class="ti-twitter"></span></a>
                  </li>
                  <li><a href="https://www.youtube.com/channel/UCAPJyZ_DIWBQSehwJqWQZtQ" target="_blank"><span class="ti-youtube"></span></a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!--=================================
            mega menu -->
      <div class="menu">
        <!-- menu start -->
        <nav id="menu" class="mega-menu">
          <!-- menu list items container -->
          <section class="menu-list-items">
            <div class="container">
              <div class="row">
                <div class="col-lg-12 col-md-12">
                  <!-- menu logo -->
                  <ul class="menu-logo">
                    <li>
                      <a href="{{route('home')}}"><img id="logo_img" src="{{ asset('frontend/images/home-logo.png') }}" alt="logo"> </a>
                    </li>
                  </ul>
                  <!-- menu links -->
                  <div class="menu-bar">
                    <ul class="menu-links">
                      <li class="hoverTrigger">
                        <a href="{{route('pages.who-we-are')}}">Who We Are </a>
                        <!-- drop down full width -->
                      </li>
                      <li class="hoverTrigger">
                        <a href="{{route('pages.what-we-do')}}">What We Do </a>
                        <!-- drop down full width -->
                      </li>
                      <li class="hoverTrigger">
                        <a href="{{route('pages.initiatives')}}">Initiatives </a>
                        <!-- drop down full width -->
                      </li>
                      <li class="hoverTrigger">
                        <a href="{{route('pages.campaigns')}}">Campaigns </a>
                        <!-- drop down full width -->
                      </li>
                      <li><a href="javascript:void(0)"> Media Centre <i class="fa fa-angle-down fa-indicator"></i></a>
                        <!-- drop down multilevel  -->
                        <ul class="drop-down-multilevel">
                          {{-- <li><a href="{{route('pages.press-releases')}}">Press Releases</a>
                          </li> --}}
                          {{-- <li><a href="{{route('pages.in-the-news')}}">In the News</a></li> --}}
                          <li><a href="{{route('pages.latest-news')}}">Latest News</a></li>
                        </ul>
                      </li>
                      <li class="hoverTrigger">
                        <a href="{{route('pages.contact')}}">Contact </a>
                        <!-- drop down full width -->
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </nav>
        <!-- menu end -->
      </div>
    </header>
    <!--=================================
        header -->
    @yield('content')

    <footer class="footer footer-one-page page-section-pt black-bg">
      <div class="container">
        <div class="row text-center">
          <div class="col-lg-12 col-md-12">
            <div class="contact-add">
              <div class="text-center">
                <img id="logo_img" src="{{ asset('frontend/images/home-logo.png') }}" width="300" alt="logo">
                <h4 class="mt-50 text-white font-size-30">Become part of our community to promote </h4>
                <h4 class="mt-10 text-white font-size-30 mb-50">Peace & Unity across the globe </h4>
              </div>
            </div>
          </div>
        </div>
        <div class="row justify-content-center">
          <div class="col-lg-4 col-md-6">
            <div class="footer-Newsletter text-center mt-30 mb-40">
              <div id="mc_embed_signup_scroll">
                <form action="php/mailchimp-action.php" method="POST" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate">
                  <div id="msg"> </div>
                  <div id="mc_embed_signup_scroll_2">
                    <input id="mce-EMAIL" class="form-control" type="text" placeholder="Email address" name="email1" value="">
                  </div>
                  <div id="mce-responses" class="clear">
                    <div class="response" id="mce-error-response" style="display:none"></div>
                    <div class="response" id="mce-success-response" style="display:none"></div>
                  </div>
                  <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                  <div style="position: absolute; left: -5000px;" aria-hidden="true">
                    <input type="text" name="b_b7ef45306f8b17781aa5ae58a_6b09f39a55" tabindex="-1" value="">
                  </div>
                  <div class="clear">
                    <button type="submit" name="submitbtn" id="mc-embedded-subscribe" class="button button-border mt-20 form-button"> Subscribe Newsletter
                    </button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
        <div class="footer-anchor text-center"><a href="{{route('pages.privacy.policy')}}">Privacy Policy</a> | <a href="{{route('pages.cookie.policy')}}">Cookie Policy</a> |
          <a href="{{route('pages.terms.of.use')}}">Terms Of Use</a> </div>
        <div class="footer-widget mt-20">
          <div class="row">
            <div class="col-lg-6 col-md-6">
              <p class="mt-15"> &copy;Copyright <span id="copyright">
                  <script>
                    document.getElementById('copyright').appendChild(document.createTextNode(new Date().getFullYear()))

                  </script>
                </span> - Global Peace & Unity for Humanity. All Rights Reserved. </p>
            </div>
            <div class="col-lg-6 col-md-6">
              <div class="social-icons color-hover float-right">
                <ul>
                  <li class="social-facebook"><a href="https://www.facebook.com/gpuhofficial" target="_blank"><i class="fa fa-facebook"></i></a></li>
                  <li class="social-instagram"><a href="https://www.instagram.com/gpuhofficial/" target="_blank"><i class="fa fa-instagram"></i> </a></li>
                  <li class="social-twitter"><a href="https://twitter.com/gpuhofficial" target="_blank"><i class="fa fa-twitter"></i></a></li>
                  <li class="social-youtube"><a href="https://www.youtube.com/channel/UCAPJyZ_DIWBQSehwJqWQZtQ" target="_blank"><i class="fa fa-youtube"></i> </a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </footer>

    <!--=================================
        footer -->
  </div>
  <!-- wrapper End -->
  <!--=================================
     javascripts -->
  <!-- jquery -->
  <script src="{{asset('frontend/js/jquery-3.4.1.min.js')}}"></script>
  <!-- All plugins -->
  <script src="{{asset('frontend/js/plugins-jquery.js')}}"></script>
  <!-- Plugins path -->
  <script>
    var plugin_path = '{{asset("frontend/js")}}/';

  </script>
  <!-- REVOLUTION JS FILES -->
  <script src="{{asset('frontend/revolution/js/jquery.themepunch.tools.min.js')}}"></script>
  <script src="{{asset('frontend/revolution/js/jquery.themepunch.revolution.min.js')}}"></script>
  <!-- SLIDER REVOLUTION 5.0 EXTENSIONS  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->
  <script src="{{asset('frontend/revolution/js/extensions/revolution.extension.actions.min.js')}}"></script>
  <script src="{{asset('frontend/revolution/js/extensions/revolution.extension.carousel.min.js')}}"></script>
  <script src="{{asset('frontend/revolution/js/extensions/revolution.extension.kenburn.min.js')}}"></script>
  <script src="{{asset('frontend/revolution/js/extensions/revolution.extension.layeranimation.min.js')}}"></script>
  <script src="{{asset('frontend/revolution/js/extensions/revolution.extension.migration.min.js')}}"></script>
  <script src="{{asset('frontend/revolution/js/extensions/revolution.extension.navigation.min.js')}}"></script>
  <script src="{{asset('frontend/revolution/js/extensions/revolution.extension.parallax.min.js')}}"></script>
  <script src="{{asset('frontend/revolution/js/extensions/revolution.extension.slideanims.min.js')}}"></script>
  <script src="{{asset('frontend/revolution/js/extensions/revolution.extension.video.min.js')}}"></script>
  <!-- revolution custom -->
  <script src="{{asset('frontend/revolution/js/revolution-custom.js')}}"></script>
  <!-- custom -->
  <script src="{{asset('frontend/js/custom.js')}}"></script>

  @stack('scripts')
</body>

</html>
