<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>GPUH | Admin Panel</title>
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    @isset($settings)
    <link rel="shortcut icon" href="{{ asset('/storage/'. $settings->favicon_icon) }}" type="image/x-icon">
    @endisset
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet"
        type="text/css">
    <link href="{{ asset('backend/css/icons/icomoon/styles.css') }}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.3/css/all.css"
        integrity="sha384-SZXxX4whJ79/gErwcOYf+zWLeJdY/qpuqC4cAa9rOGUstPomtqpuNWT9wdPEn2fk" crossorigin="anonymous">
    <link href="{{ asset('backend/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('backend/css/bootstrap_limitless.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('backend/css/layout.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('backend/css/core.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('backend/css/components.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('backend/css/colors.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('backend/css/custom.css') }}" rel="stylesheet" type="text/css">
    @yield('head')
    <style>
        ul.pagination {
            justify-content: center;
            margin-bottom: 10px;
        }
    </style>
</head>

<body>
    <div class="navbar navbar-expand-md navbar-dark d-flex align-items-center flex-row" style="padding:12px 15px;">
        <div class="navbar-brand p-0" style="min-width:auto;">
            <a href="{{ route('admin.home') }}">
                <img src="{{ asset('/logo.png') }}">
            </a>
        </div>
        @isset($settings)
        <div class="text-white">
            <span class="m-0 h4">
                {{ $settings->site_name }}
            </span>
        </div>
        @else
        <div class="text-white">
            <span class="m-0 h4 font-weight-bold">
                Global Peace & Unity for Humanity
            </span>
        </div>
        @endisset
        <div class="d-md-none">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-mobile">
                <i class="icon-tree5"></i>
            </button>
            <button class="navbar-toggler sidebar-mobile-main-toggle" type="button">
                <i class="icon-paragraph-justify3"></i>
            </button>
        </div>

        <div class="collapse navbar-collapse" id="navbar-mobile">
            <span class="navbar-text ml-md-3 mr-md-auto"></span>
            <ul class="navbar-nav">
                <li class="nav-item dropdown dropdown-user">
                    <a href="#" class="navbar-nav-link dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-user text-white"></i>
                        <span>
                            @auth
                            {{ Auth::user()->username }}
                            @endauth
                        </span>
                    </a>

                    <div class="dropdown-menu dropdown-menu-right">
                        <a href="{{ route('admin.change-password.edit') }}" class="dropdown-item"><i
                                class="icon-cog5"></i> Change Password</a>
                        <a href="{{ route('admin.logout') }}" class="dropdown-item"
                            onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                            <i class="icon-switch2"></i>
                            Logout
                        </a>
                        <form id="logout-form" action="{{ route('admin.logout') }}" method="POST"
                            style="display: none;">
                            @csrf
                        </form>
                    </div>
                </li>
            </ul>
        </div>
    </div>
    <div class="page-content">
        <div class="sidebar sidebar-dark sidebar-main sidebar-expand-md">
            <div class="sidebar-content">
                <div class="card card-sidebar-mobile">
                    <ul class="nav nav-sidebar" data-nav-type="accordion">

                        <li class="nav-item  {{ (request()->routeIs('index')) ? 'nav-item-open' : '' }}">
                            <a href="{{ route('admin.home') }}" class="nav-link">
                                <i class="icon-home4"></i>
                                <span>
                                    Dashboard
                                </span>
                            </a>
                        </li>
                        <li
                            class="nav-item nav-item-submenu  {{ (request()->routeIs('admin.initiatives.*')) ? 'nav-item-open' : '' }}">
                            <a href="#" class="nav-link">
                                <i class="fas fa-shoe-prints"></i>
                                <span>Initiatives</span>
                            </a>
                            <ul class="nav nav-group-sub"
                                style="{{ (request()->routeIs('admin.initiatives.*')) ? "display:block;" : '' }}"
                                data-submenu-title="Initiatives">
                                <li class="nav-item">
                                    <a href="{{ route('admin.initiatives.create') }}"
                                        class="nav-link  {{ (request()->routeIs('admin.initiatives.create')) ? 'active' : '' }}">Add
                                        New
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ route('admin.initiatives.index') }}"
                                        class="nav-link  {{ (request()->routeIs('admin.initiatives.index')) ? 'active' : '' }}">
                                        View All
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li
                            class="nav-item nav-item-submenu  {{ (request()->routeIs('admin.press-releases.*')) ? 'nav-item-open' : '' }}">
                            <a href="#" class="nav-link">
                                <i class="icon-list"></i>
                                <span>Press Releases</span>
                            </a>
                            <ul class="nav nav-group-sub"
                                style="{{ (request()->routeIs('admin.press-releases.*')) ? "display:block;" : '' }}"
                                data-submenu-title="Press Releases">
                                <li class="nav-item">
                                    <a href="{{ route('admin.press-releases.create') }}"
                                        class="nav-link  {{ (request()->routeIs('admin.press-releases.create')) ? 'active' : '' }}">Add
                                        New
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ route('admin.press-releases.index') }}"
                                        class="nav-link  {{ (request()->routeIs('admin.press-releases.index')) ? 'active' : '' }}">View
                                        All
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li
                            class="nav-item nav-item-submenu  {{ (request()->routeIs('admin.news.*')) ? 'nav-item-open' : '' }}">
                            <a href="#" class="nav-link">
                                <i class="icon-newspaper"></i>
                                <span>News</span>
                            </a>
                            <ul class="nav nav-group-sub"
                                style="{{ (request()->routeIs('admin.news.*')) ? "display:block;" : '' }}"
                                data-submenu-title="Press Releases">
                                <li class="nav-item">
                                    <a href="{{ route('admin.news.create') }}"
                                        class="nav-link  {{ (request()->routeIs('admin.news.create')) ? 'active' : '' }}">Add
                                        New
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ route('admin.news.index') }}"
                                        class="nav-link  {{ (request()->routeIs('admin.news.index')) ? 'active' : '' }}">View
                                        All
                                    </a>
                                </li>
                            </ul>
                        </li>

                        <li class="nav-item  {{ (request()->routeIs('admin.site-settings.edit')) ? 'nav-item-open' : '' }}">
                            <a href="{{ route('admin.site-settings.edit') }}" class="nav-link">
                                <i class="icon-home4"></i>
                                <span>
                                    Site Settings
                                </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="content-wrapper">

            @yield('content')

            <div class="navbar navbar-expand-lg navbar-light">
                <div class="text-center d-lg-none w-100">
                    <button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse"
                        data-target="#navbar-footer">
                        <i class="icon-unfold mr-2"></i>
                        Footer
                    </button>
                </div>
                <div class="navbar-collapse collapse" id="navbar-footer">
                    <span class="navbar-text">
                        &copy; Copyright 2021 - Global Peace & Unity for Humanity. All Rights
                        Reserved.
                    </span>
                </div>
            </div>
        </div>
    </div>

    <!-- Core JS files -->
    <script src="{{ asset('backend/js/main/jquery.min.js') }}"></script>
    <script src="{{ asset('backend/js/tinymce/js/jquery.tinymce.min.js') }}"></script>
    <script src="{{ asset('backend/js/tinymce/js/tinymce.min.js') }}"></script>
    <script src="{{ asset('backend/js/tinymce/js/init-tinymce.js') }}"></script>
    <script src="{{ asset('backend/js/plugins/visualization/d3/d3.min.js') }}"></script>
    <script src="{{ asset('backend/js/plugins/visualization/d3/d3_tooltip.js') }}"></script>
    <script src="{{ asset('backend/js/plugins/forms/styling/switchery.min.js') }}"></script>
    <script src="{{ asset('backend/js/plugins/forms/selects/bootstrap_multiselect.js') }}"></script>
    <script src="{{ asset('backend/js/plugins/ui/moment/moment.min.js') }}"></script>
    <script src="{{ asset('backend/js/demo_pages//form_multiselect.js') }}"></script>
    <script src="{{ asset('backend/js/plugins/pickers/daterangepicker.js') }}"></script>
    <script src="{{ asset('backend/js/demo_pages/dashboard.js') }}"></script>
    <script src="{{ asset("backend/assets/js/main/bootstrap.bundle.min.js") }}"></script>
    <script src="{{ asset("backend/assets/js/plugins/loaders/blockui.min.js") }}"></script>
    <script src="{{ asset("backend/assets/js/plugins/forms/styling/uniform.min.js") }}"></script>
    <script src="{{ asset("backend/assets/js/plugins/notifications/pnotify.min.js") }}"></script>
    <script src="{{ asset("backend/assets/js/plugins/forms/selects/bootstrap_multiselect.js") }}"></script>
    <script src="{{ asset("backend/assets/js/app.js") }}" defer></script>
    <script src="{{  asset("backend/js/demo_pages/dashboard.js") }}"></script>
    <script src="{{asset('backend/js/plugins/tables/datatables/datatables.min.js')}}"></script>
    <script src="{{ asset('backend/js/plugins/tables/datatables/extensions/responsive.min.js') }}"></script>
    <script src="{{ asset('backend/js/demo_pages/datatables_responsive.js') }}"></script>
    <script src="{{ asset("backend/js/demo_pages/datatables_basic.js") }}" defer></script>
    <script src="{{ asset('backend/js/demo_pages/datatables_advanced.js') }}"></script>

    @stack('script')
    <script>
        var copyButtons = document.getElementsByClassName('copy-button');

    for (var i = 0; i < copyButtons.length; i++) {
      var element = copyButtons[i];
      element.addEventListener('click', copyToClipboard);
      element.addEventListener('mouseout', resetCopyButton);
    }

    function copyToClipboard(e) {
      this.innerHTML = "Copied";
      var copyText = document.getElementById(this.dataset.target);
      copyText.select();
      copyText.setSelectionRange(0, 99999)
      document.execCommand("copy");
      this.classList.remove("btn-outline-primary");
      this.classList.add("btn-success");
    }

    function resetCopyButton(e) {
      if (this.innerHTML != 'Copy') {
        this.innerHTML = "Copy";
        this.classList.add("btn-outline-primary");
        this.classList.remove("btn-success");
      }
    }

    </script>
    @stack('scripts')
</body>

</html>
