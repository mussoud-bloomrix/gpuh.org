@extends('admin.layouts.layout')

@section('heading')
<h4>
    <a href="{{ route('admin.initiatives.campaigns.index', $initiative->id) }}">
        <i class="icon-arrow-left52 mr-2"></i>
    </a>
    <span class="font-weight-semibold">Home - Campaigns</span> - Create
</h4>
@endsection

@section('breadcrumbs')
<div class="breadcrumb">
    <a href="{{ route('admin.home') }}" class="breadcrumb-item">
        <i class="icon-home2 mr-2"></i> Home
    </a>
    <span class="breadcrumb-item">
        <a href="{{ route('admin.initiatives.index') }}"> Initiative
        </a>
    </span>
    <span class="breadcrumb-item">
        <a href="{{ route('admin.initiatives.campaigns.index', $initiative->id) }}"> Campaigns
        </a>
    </span>
    <span class="breadcrumb-item active">Edit</span>
</div>
@endsection

@section('content')
@include('admin.partials.header')
<div class="content">
    @include('common.partials.flash')
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">Edit Campaign</h5>
        </div>
        <div class="card-body">
            <form method="POST"
                action="{{route('admin.initiatives.campaigns.update', [$initiative->id, $campaign->id] )}}"
                enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <fieldset class="mb-3">
                    <legend class="text-uppercase font-size-sm font-weight-bold">
                        Edit the form below to update the campaign
                    </legend>

                    <div class="form-group">
                        <label class="@error('name') text-danger @enderror">Name: <span class="text-red">*</span>
                        </label>
                        <input type="text" class="form-control @error('name') border-danger @enderror" name="name"
                            value="{{ old('name', $campaign->name) }}" required>
                        @error('name')
                        <span class="form-text text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label class="@error('header_image') text-danger @enderror">Header image: <span
                                class="text-red">*</span>
                        </label>
                        <div class="" style="position: relative;margin-bottom: inherit;">
                            <div class="d-inline-block" style="position:relative;margin-bottom: inherit; ">
                                <img src="{{ asset('/storage/'.$campaign->header_image) }}" height="100px"
                                    width="100px">
                            </div>
                            <input type="file" accept=".jpeg,.png,.bmp,.gif,.svg,.webp" name="header_image">
                            <span class="form-text text-muted">Accepted formats: jpeg, png, bmp, gif, svg, or
                                webp</span>
                        </div>

                        @error('header_image')
                        <span class="form-text text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label class="@error('feature_image') text-danger @enderror">Feature image: <span
                                class="text-red">*</span>
                        </label>
                        <div class="" style="position: relative;margin-bottom: inherit;">
                            <div class="d-inline-block" style="position:relative;margin-bottom: inherit; ">
                                <img src="{{ asset('/storage/'.$campaign->feature_image) }}" height="100px" width="100px">
                            </div>
                            <input type="file" accept=".jpeg,.png,.bmp,.gif,.svg,.webp" name="feature_image">
                            <span class="form-text text-muted">Accepted formats: jpeg, png, bmp, gif, svg, or webp</span>
                        </div>
                        @error('feature_image')
                            <span class="form-text text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label class="@error('excerpt') text-danger @enderror">
                            Excerpt: <span class="text-danger">*</span>
                        </label>
                        <textarea class="form-control @error('excerpt') border-danger @enderror" cols="30" rows="5" name="excerpt">{{ old('excerpt', $campaign->excerpt) }}</textarea>
                        @error('excerpt')
                            <span class="form-text text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label class="@error('body') text-danger @enderror">Body: <span class="text-red">*</span>
                        </label>
                        <textarea class="form-control tinymce" cols="30" rows="5" name="body">{{ old('body', $campaign->body) }}</textarea>
                        @error('body')
                        <span class="form-text text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                </fieldset>

                <fieldset class="mb-3">
                    <legend class="text-uppercase font-size-sm font-weight-bold">
                        META INFORMATION
                    </legend>

                    <div class="form-group">
                        <label class="@error('meta_title') text-danger @enderror">Meta title: <span
                                class="text-red">*</span>
                        </label>
                        <input type="text" class="form-control @error('meta_title') border-danger @enderror"
                            name="meta_title" value="{{ old('meta_title', $campaign->meta_title) }}" required>
                        @error('meta_title')
                        <span class="form-text text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label class="@error('meta_keywords') text-danger @enderror">Meta keywords:
                        </label>
                        <input type="text" class="form-control @error('meta_keywords') border-danger @enderror"
                            name="meta_keywords" value="{{ old('meta_keywords', $campaign->meta_keywords) }}">
                        @error('meta_keywords')
                        <span class="form-text text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label class="@error('meta_description') text-danger @enderror">Meta description:
                        </label>
                        <textarea name="meta_description" id="meta_description" cols="30" rows="5"
                            class="form-control @error('meta_description') border-danger @enderror">
                            {{ old('meta_description', $campaign->meta_description) }}</textarea>
                        @error('meta_description')
                        <span class="form-text text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                </fieldset>

                <div class="text-right">
                    <button type="submit" class="btn btn-primary">Save changes
                        <i class="icon-paperplane ml-2"></i>
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script src="{{  asset('backend/js/demo_pages/form_layouts.js') }}"></script>
<script src="{{  asset('backend/js/plugins/forms/selects/select2.min.js') }}"></script>
<script src="{{  asset('backend/js/plugins/forms/styling/uniform.min.js') }}"></script>
@endpush
