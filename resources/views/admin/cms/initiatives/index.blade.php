@extends('admin.layouts.layout')

@section('heading')
<h4>
    <a href="{{ route('admin.home') }}">
        <i class="icon-arrow-left52 mr-2"></i>
    </a>
    <span class="font-weight-semibold">Home - Initiatives</span> - View All
</h4>
@endsection

@section('breadcrumbs')
<div class="breadcrumb">
    <a href="{{ route('admin.home') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
    <span class="breadcrumb-item"><a href="{{ route('admin.initiatives.index') }}"> Initiatives </a></span>
    <span class="breadcrumb-item active">View All</span>
</div>
@endsection

@section('content')
@include('admin.partials.header')
<div class="content">
    @include('common.partials.flash')
    <div class="card has-table">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">Initiatives</h5>
            <div class="header-elements">
                <a href="{{ route('admin.initiatives.create') }}" class="mt-2 btn btn-primary">
                    Add New
                </a>
            </div>
        </div>
        <table class="table datatable-basic">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Created At</th>
                    <th>Last Modified</th>
                    <th class="text-center">Actions</th>
                </tr>
            </thead>
            <tbody>
                @forelse($initiatives as $key => $initiative)
                <tr>
                    <td>{{$key+1}}</td>
                    <td>{{$initiative->name}}</td>
                    <td>{{$initiative->created_at}}</td>
                    <td>{{$initiative->updated_at }}</td>
                    <td class="text-center">
                        <div class="list-icons">
                            <div class="dropdown">
                                <a href="#" class="list-icons-item" data-toggle="dropdown">
                                    <i class="icon-menu9"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <a href="{{route('admin.initiatives.edit',  $initiative->id )}}"
                                        class="dropdown-item"><i class="icon-pencil5"></i> Edit
                                    </a>
                                    <a href="{{route('admin.initiatives.campaigns.index',  $initiative->id )}}"
                                        class="dropdown-item"><i class="fas fa-bullseye"></i> View Campaigns
                                    </a>
                                    <form action='{{route('admin.initiatives.destroy',  [$initiative->id] )}}'
                                        method='POST'>
                                        @csrf
                                        @method('DELETE')
                                        <button type='submit' class="dropdown-item"><i class="icon-trash"></i>
                                            Delete</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                @empty
                <tr>
                    <td colspan="12">
                        <div class="alert alert-info text-center">
                            No Initiative Added So Far
                            <br>
                            <a href="{{ route('admin.initiatives.create') }}" class="mt-2 btn btn-primary">
                                Add New
                            </a>
                        </div>
                    </td>
                </tr>
                @endforelse
            </tbody>
        </table>
    </div>
</div>
@endsection
