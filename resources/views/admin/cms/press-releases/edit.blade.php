@extends('admin.layouts.layout')

@section('heading')
<h4>
    <a href="{{ route('admin.press-releases.index') }}">
        <i class="icon-arrow-left52 mr-2"></i>
    </a>
    <span class="font-weight-semibold">Home - Press Releases</span> - Create
</h4>
@endsection

@section('breadcrumbs')
<div class="breadcrumb">
    <a href="{{ route('admin.home') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
    <span class="breadcrumb-item"><a href="{{ route('admin.press-releases.index') }}"> Press Releases </a></span>
    <span class="breadcrumb-item active">Edit</span>
</div>
@endsection

@section('content')
@include('admin.partials.header')
<div class="content">
    @include('common.partials.flash')
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">Edit Press Release</h5>
        </div>
        <div class="card-body">
            <form method="POST" action="{{route('admin.press-releases.update', $pressRelease)}}"
                enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <fieldset class="mb-3">
                    <legend class="text-uppercase font-size-sm font-weight-bold">
                        Edit the form below to update the press release
                    </legend>

                    <div class="form-group">
                        <label class="@error('title') text-danger @enderror">Title: <span class="text-red">*</span>
                        </label>
                        <input type="text" class="form-control @error('title') border-danger @enderror" name="title"
                            value="{{ old('title', $pressRelease->title) }}" >
                        @error('title')
                        <span class="form-text text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label class="@error('featured_image') text-danger @enderror">Featured image: <span
                                class="text-red">*</span>
                        </label>
                        <div class='mb-1'>
                            <img src="{{ asset('storage/'.$pressRelease->featured_image) }}" width="200px">
                        </div>
                        <input type="file" class="form-control-file @error('featured_image') border-danger @enderror"
                            accept=".jpeg,.png,.bmp,.gif,.svg,.webp" name="featured_image"
                            value="{{ old('featured_image') }}">
                        <span class="form-text text-muted">Accepted formats: jpeg, png, bmp, gif, svg, or
                            webp</span>
                        @error('featured_image')
                        <span class="form-text text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label class="@error('header_image') text-danger @enderror">Header image: <span
                                class="text-red">*</span>
                        </label>
                        <div class='mb-1'>
                            <img src="{{ asset('storage/'.$pressRelease->header_image) }}" width="200px">
                        </div>
                        <input type="file" class="form-control-file @error('header_image') border-danger @enderror"
                            accept=".jpeg,.png,.bmp,.gif,.svg,.webp" name="header_image"
                            value="{{ old('header_image') }}">
                        <span class="form-text text-muted">Accepted formats: jpeg, png, bmp, gif, svg, or
                            webp</span>
                        @error('header_image')
                        <span class="form-text text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label class="@error('campaigns') text-danger @enderror">Campaigns: <span
                                class="text-red">*</span>
                        </label>
                        <select class='form-control select2 @error("campaigns") border-danger @enderror'
                            name='campaigns[]' multiple >
                            <option value="">Select an option</option>
                            @foreach ($campaigns as $campaign)
                            <option
                                {{ in_array($campaign->id, old('campaigns', $pressRelease->campaigns()->pluck('campaigns.id')->toArray())) ? 'selected' : '' }}
                                value="{{ $campaign->id }}">
                                {{ $campaign->name }}</option>
                            @endforeach
                        </select>
                        @error('campaigns')
                        <span class="form-text text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label class="@error('date') text-danger @enderror">Date:
                            <span class="text-red">*</span>
                        </label>
                        <input type="date" class="form-control @error('date') border-danger @enderror" name="date"
                            value="{{  \Carbon\Carbon::parse(old('date', $pressRelease->date))->format('Y-m-d') }}"
                            >
                        @error('date')
                        <span class="form-text text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label class="@error('excerpt') text-danger @enderror">Excerpt: <span class="text-red">*</span>
                        </label>
                        <textarea class="form-control" rows="5" name="excerpt" value="">{{ old('excerpt', $pressRelease->excerpt) }}</textarea>
                        @error('excerpt')
                        <span class="form-text text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label class="@error('video_url') text-danger @enderror">Video url:
                        </label>
                        <input type="text" class="form-control @error('video_url') border-danger @enderror" name="video_url"
                            value="{{ old('video_url', $pressRelease->video_url) }}" >
                        @error('video_url')
                        <span class="form-text text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label class="@error('image') text-danger @enderror">Image:</label>
                        <div class='mb-1'>
                            <img src="{{ asset('storage/'.$pressRelease->image) }}" width="200px">
                        </div>
                        <input type="file" class="form-control-file @error('image') border-danger @enderror"
                            accept=".jpeg,.png,.bmp,.gif,.svg,.webp" name="image"
                            value="{{ old('image') }}">
                        <span class="form-text text-muted">Accepted formats: jpeg, png, bmp, gif, svg, or
                            webp</span>
                        @error('image')
                        <span class="form-text text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label class="@error('introduction') text-danger @enderror">Introduction: <span class="text-red">*</span>
                        </label>
                        <textarea class="form-control" cols="30" rows="5" name="introduction" value="">{{ old('introduction', $pressRelease->introduction) }}</textarea>
                        @error('introduction')
                        <span class="form-text text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label class="@error('slider_images') text-danger @enderror">Slider images: <span
                                class="text-red">*</span>
                        </label>
                        <div class='mb-1'>
                            @if($pressRelease->sliderImages())
                                @foreach ($pressRelease->sliderImages() as $slider_image)
                                    <img src="{{ asset('storage/'.$slider_image) }}" width="100px">
                                @endforeach
                            @endif

                        </div>
                        <input type="file" class="form-control-file @error('slider_images') border-danger @enderror"
                            accept=".jpeg,.png,.bmp,.gif,.svg,.webp" name="slider_images[]"
                            value="{{ old('slider_images') }}" multiple>
                        <span class="form-text text-muted">Accepted formats: jpeg, png, bmp, gif, svg, or
                            webp</span>
                        @error('slider_images')
                        <span class="form-text text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label class="@error('body') text-danger @enderror">Body: <span class="text-red">*</span>
                        </label>
                        <textarea class="form-control tinymce" cols="30" rows="5" name="body" value="">{{ old('body', $pressRelease->body) }}</textarea>
                        @error('body')
                        <span class="form-text text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                </fieldset>

                <fieldset class="mb-3">
                    <legend class="text-uppercase font-size-sm font-weight-bold">
                        Meta Information
                    </legend>

                    <div class="form-group">
                        <label class="@error('title') text-danger @enderror">Meta title: <span class="text-red">*</span>
                        </label>
                        <input type="text" class="form-control @error('meta_title') border-danger @enderror"
                            name="meta_title" value="{{ old('meta_title', $pressRelease->meta_title) }}" >
                        @error('meta_title')
                        <span class="form-text text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label class="@error('title') text-danger @enderror">Meta keywords:
                        </label>
                        <input type="text" class="form-control @error('meta_keywords') border-danger @enderror"
                            name="meta_keywords" value="{{ old('meta_keywords', $pressRelease->meta_keywords) }}">
                        @error('meta_keywords')
                        <span class="form-text text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label class="@error('title') text-danger @enderror">Meta description:
                        </label>
                        <textarea name="meta_description" id="meta_description" cols="30" rows="5"
                            class="form-control @error('meta_description') border-danger @enderror">{{ old('meta_description', $pressRelease->meta_description) }}</textarea>
                        @error('meta_description')
                        <span class="form-text text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                </fieldset>

                <div class="text-right">
                    <button type="submit" class="btn btn-primary">
                        Submit
                        <i class="icon-paperplane ml-2"></i>
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script src="{{ asset('backend/js/plugins/forms/selects/select2.min.js') }}"></script>
<script src="{{ asset('backend/js/plugins/forms/styling/uniform.min.js') }}"></script>
<script>
    $('.select2').select2({
        placeholder: 'Select an option'
    });
</script>
@endpush
