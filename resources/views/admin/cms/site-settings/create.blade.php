@extends('admin.layouts.layout')

@section('heading')
    <h4>
        <a href="{{ route('admin.home') }}">
            <i class="icon-arrow-left52 mr-2"></i>
        </a>
        <span class="font-weight-semibold">Home - General Settings</span>
    </h4>
@endsection

@section('breadcrumbs')
    <div class="breadcrumb">
        <a href="{{ route('admin.home') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
        <span class="breadcrumb-item">General Settings</span>
    </div>
@endsection

@section('content')
    @include('admin.partials.header')
    <div class="content">
        @include('common.partials.flash')

        <div class="card">
            <div class="card-header header-elements-inline">
                <h5 class="card-title">Site Settings</h5>
            </div>

            <div class="card-body">
                <form action="{{ route('admin.site-settings.store') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label style="display: block;"
                            class="font-weight-semibold @error('logo') text-danger @enderror">Logo <span class="text-red">*</span>
                        </label>
                        <div class="d-inline-block" style="position: relative;margin-bottom: inherit;">
                        <input type="file" accept=".jpeg,.png,.bmp,.gif,.svg,.webp" name="logo" value="{{ old('logo') }}">
                        <span class="form-text text-muted">Accepted formats: jpeg, png, bmp, gif, svg, or webp</span>
                        @error('logo')
                            <span class="form-text text-danger">{{ $message }}</span>
                        @enderror
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="font-weight-semibold @error('facebook_link') text-danger @enderror">Facebook Link</label>
                        <div class="form-group-feedback form-group-feedback-right">
                            <input type="text" name="facebook_link" value="{{ old('facebook_link') }}"
                                class="form-control @error('facebook_link') border-danger @enderror">
                            @error('facebook_link')
                            <div class="form-control-feedback text-danger">
                                <i class="icon-cancel-circle2"></i>
                            </div>
                            @enderror
                        </div>
                        @error('facebook_link')
                            <span class="form-text text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label class="font-weight-semibold @error('linkedin_link') text-danger @enderror">Linkedin Link</label>
                        <div class="form-group-feedback form-group-feedback-right">
                            <input type="text" name="linkedin_link" value="{{ old('linkedin_link') }}"
                                class="form-control @error('linkedin_link') border-danger @enderror">
                            @error('linkedin_link')
                            <div class="form-control-feedback text-danger">
                                <i class="icon-cancel-circle2"></i>
                            </div>
                            @enderror
                        </div>
                        @error('linkedin_link')
                            <span class="form-text text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label class="font-weight-semibold @error('youtube_link') text-danger @enderror">Youtube Link</label>
                        <div class="form-group-feedback form-group-feedback-right">
                            <input type="text" name="youtube_link" value="{{ old('youtube_link') }}"
                                class="form-control @error('youtube_link') border-danger @enderror">
                            @error('youtube_link')
                            <div class="form-control-feedback text-danger">
                                <i class="icon-cancel-circle2"></i>
                            </div>
                            @enderror
                        </div>
                        @error('youtube_link')
                            <span class="form-text text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label class="font-weight-semibold @error('twitter_link') text-danger @enderror">Twitter Link</label>
                        <div class="form-group-feedback form-group-feedback-right">
                            <input type="text" name="twitter_link" value="{{ old('twitter_link') }}"
                                class="form-control @error('twitter_link') border-danger @enderror">
                            @error('twitter_link')
                            <div class="form-control-feedback text-danger">
                                <i class="icon-cancel-circle2"></i>
                            </div>
                            @enderror
                        </div>
                        @error('twitter_link')
                            <span class="form-text text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label class="font-weight-semibold @error('email') text-danger @enderror">Email <span class="text-red">*</span></label>
                        <div class="form-group-feedback form-group-feedback-right">
                            <input type="email" name="email" value="{{ old('email') }}"
                                class="form-control @error('email') border-danger @enderror">
                            @error('email')
                            <div class="form-control-feedback text-danger">
                                <i class="icon-cancel-circle2"></i>
                            </div>
                            @enderror
                        </div>
                        @error('email')
                            <span class="form-text text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="text-right">
                        <button type="submit" class="btn btn-primary">Submit <i class="icon-paperplane ml-2"></i></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
