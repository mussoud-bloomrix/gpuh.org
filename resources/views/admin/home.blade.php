@extends('admin.layouts.layout')

@section('heading')
<h4>
    <span class="font-weight-semibold">Home</span> - Dashboard
</h4>
@endsection

@section('breadcrumbs')
<div class="breadcrumb">
    <a href="{{ route('admin.home') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
    <span class="breadcrumb-item active">Dashboard</span>
</div>
@endsection

@section('content')
@include('admin.partials.header')
<!-- Content area -->
<div class="content">
    @include('common.partials.flash')
    <!-- Dashboard content -->
    <div class="row">
        <div class="col-xl-12">
            <!-- Stats boxes -->
            <div class="row justify-content-center">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-6">
                    <div class="card bg-teal-400" style="min-height:9rem;max-height:10rem;">
                        <a href="{{ route('admin.initiatives.index') }}" class="text-white">
                            <div class="text-center card-body">
                                <div>
                                    <h1 class="font-weight-semibold mb-0">
                                        <i class="fas fa-shoe-prints fa-2x"></i>
                                    </h1>
                                </div>
                                <div class="pt-2">
                                    <h1 class="font-weight-semibold mb-0">Initiatives</h1>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>

                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-6">
                    <div class="card bg-teal-400" style="min-height:9rem;max-height:10rem;">
                        <a href="{{ route('admin.press-releases.index') }}" class="text-white">
                            <div class="text-center card-body">
                                <div>
                                    <h1 class="font-weight-semibold mb-0"><i class="icon-3x mr-1 icon-list"></i>
                                    </h1>
                                </div>
                                <div class="pt-2">
                                    <h1 class="font-weight-semibold mb-0">Press Releases</h1>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>

                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-6">
                    <div class="card bg-teal-400" style="min-height:9rem;max-height:10rem;">
                        <a href="{{ route('admin.news.index') }}" class="text-white">
                            <div class="text-center card-body">
                                <div>
                                    <h1 class="font-weight-semibold mb-0"><i class="icon-3x mr-1 icon-newspaper"></i>
                                    </h1>
                                </div>
                                <div class="pt-2">
                                    <h1 class="font-weight-semibold mb-0">
                                        News
                                    </h1>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>

                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-6">
                    <div class="card bg-teal-400" style="min-height:9rem;max-height:10rem;">
                        <a href="{{ route('admin.site-settings.edit') }}" class="text-white">
                            <div class="text-center card-body">
                                <div>
                                    <h1 class="font-weight-semibold mb-0"><i class="icon-3x mr-1 icon-gear"></i>
                                    </h1>
                                </div>
                                <div class="pt-2">
                                    <h1 class="font-weight-semibold mb-0">Site Settings</h1>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <!-- /Stats boxes -->
            </div>
        </div>
        <!-- /dashboard content -->
    </div>
    <!-- /content area -->
</div>
@endsection
